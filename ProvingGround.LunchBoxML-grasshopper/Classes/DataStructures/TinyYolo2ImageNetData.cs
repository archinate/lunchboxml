﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML.Data;

public class ImageNetData
{
    [LoadColumn(0)]
    public string ImagePath;

    [LoadColumn(1)]
    public string Label;

    public static IEnumerable<ImageNetData> ReadFromFile(string imageFolder)
    {
        return Directory
            .GetFiles(imageFolder)
            .Where(filePath => Path.GetExtension(filePath) != ".md")
            .Select(filePath => new ImageNetData { ImagePath = filePath, Label = Path.GetFileName(filePath) });
    }

    public static IEnumerable<ImageNetData> ReadFile(string filePath)
    {
        var image = new ImageNetData { ImagePath = filePath, Label = Path.GetFileName(filePath) };
        return Enumerable.Repeat(image, 1);
    }
}