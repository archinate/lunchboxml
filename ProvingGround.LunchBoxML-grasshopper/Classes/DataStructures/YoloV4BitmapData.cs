﻿
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.Image;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace ProvingGround.MachineLearning.Classes.DataStructures
{
    public class YoloV4BitmapData
    {
        [ColumnName("bitmap")]
        [ImageType(416, 416)]
        public Bitmap Image { get; set; }

        [ColumnName("width")]
        public float ImageWidth => Image.Width;

        [ColumnName("height")]
        public float ImageHeight => Image.Height;


        public static IEnumerable<YoloV4BitmapData> ReadFile(string filePath)
        {
            var image = new ImageNetData { ImagePath = filePath, Label = Path.GetFileName(filePath) };
            return (IEnumerable<YoloV4BitmapData>)Enumerable.Repeat(image, 1);
        }
    }
}
