﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Classes
{
    public enum BinaryClassificationTrainer
    {
        AveragedPerceptron,
        FastForest,
        FastTree,
        FieldAwareFactorizationMachine,
        Gam,
        LbfgsLogisticRegression,
        LdSvm,
        LightGbm,
        LinearSvm,
        SdcaLogisticRegression,
        SdcaNonCalibrated,
        SgdCalibrated,
        SgdNonCalibrated,
        SymbolicSgdLogisticRegression,
        Count
    }

    public class BinaryClassificationTrainerBase
    {
        private BinaryClassificationTrainer _trainer;
        public BinaryClassificationTrainer Trainer
        {
            get => _trainer;
            set => _trainer = value;
        }
        public BinaryClassificationTrainerBase(BinaryClassificationTrainer _trainer)
        {
            Trainer = _trainer;
        }
    }
    public class AveragedPerceptronBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public AveragedPerceptronBinaryClassificationTrainer(float _learningRate,
            bool _decreaseLearningRate,
            float _L2,
            int _numberOfIterations) :
            base(BinaryClassificationTrainer.AveragedPerceptron)
        {
            LearningRate = _learningRate;
            DecreaseLearningRate = _decreaseLearningRate;
            L2Regularization = _L2;
            NumberOfIterations = _numberOfIterations;
        }

        private float _learningRate;
        private int _numberOfIterations;
        private bool _decreaseLearningRate;
        private float _L2Regularization;
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
        public bool DecreaseLearningRate
        {
            get => _decreaseLearningRate;
            set => _decreaseLearningRate = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
    }

    public class FastForestBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public FastForestBinaryClassificationTrainer(int _numberOfLeaves,
            int _numberOfTrees,
            int _minExampleCountPerLeaf) :
            base(BinaryClassificationTrainer.FastForest)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfTrees = _numberOfTrees;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
        }

        private int _numberOfLeaves;
        private int _numberOfTrees;
        private int _minExampleCountPerLeaf;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfTrees
        {
            get => _numberOfTrees;
            set => _numberOfTrees = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
    }
    public class FastTreeBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public FastTreeBinaryClassificationTrainer(int _numberOfLeaves,
            int _numberOfTrees,
            int _minExampleCountPerLeaf,
            float _learningRate) :
            base(BinaryClassificationTrainer.FastTree)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfTrees = _numberOfTrees;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
            LearningRate = _learningRate;
        }

        private int _numberOfLeaves;
        private int _numberOfTrees;
        private int _minExampleCountPerLeaf;
        private float _learningRate;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfTrees
        {
            get => _numberOfTrees;
            set => _numberOfTrees = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }
    public class FieldAwareFactorizationMachineBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public FieldAwareFactorizationMachineBinaryClassificationTrainer() :
            base(BinaryClassificationTrainer.FieldAwareFactorizationMachine)
        {
 
        }
    }
    public class GAMBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public GAMBinaryClassificationTrainer(int _numberOfIterations,
            int _maxBinCountPerFeature,
            float _learningRate) :
            base(BinaryClassificationTrainer.Gam)
        {
            NumberOfIterations = _numberOfIterations;
            MaxBinCountPerFeature = _maxBinCountPerFeature;
            LearningRate = _learningRate;
        }

        private int _numberOfIterations;
        private int _maxBinCountPerFeature;
        private float _learningRate;
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public int MaxBinCountPerFeature
        {
            get => _maxBinCountPerFeature;
            set => _maxBinCountPerFeature = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }
    public class LbfgsLogisticRegressionBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public LbfgsLogisticRegressionBinaryClassificationTrainer(bool _enforceNonNegativity,
            float _L1,
            float _L2,
            int _historySize,
            float optimizationTolerance) :
            base(BinaryClassificationTrainer.LbfgsLogisticRegression)
        {
            EnforceNonNegativity = _enforceNonNegativity;
            L1Regularization = _L1;
            L2Regularization = _L2;
            OptimizationTolerance = optimizationTolerance;
            HistorySize = _historySize;
        }

        private bool _enforceNonNegativity;
        private float _L1Regularization;
        private float _L2Regularization;
        private int _historySize;
        private float _optimizationTolerance;
        public bool EnforceNonNegativity
        {
            get => _enforceNonNegativity;
            set => _enforceNonNegativity = value;
        }
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }

        public float OptimizationTolerance
        {
            get => _optimizationTolerance;
            set => _optimizationTolerance = value;
        }
        public int HistorySize
        {
            get => _historySize;
            set => _historySize = value;
        }
    }
    public class LDSVMBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public LDSVMBinaryClassificationTrainer(int _numberOfIterations,
            int _treeDepth,
            bool _useBias,
            bool _useCachedData) :
            base(BinaryClassificationTrainer.LdSvm)
        {
            NumberOfIterations = _numberOfIterations;
            TreeDepth = _treeDepth;
            UseBias = _useBias;
            UseCachedData = _useCachedData;
        }

        private int _numberOfIterations;
        private int _treeDepth;
        private bool _useBias;
        private bool _useCachedData;
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public int TreeDepth
        {
            get => _treeDepth;
            set => _treeDepth = value;
        }
        public bool UseBias
        {
            get => _useBias;
            set => _useBias = value;
        }
        public bool UseCachedData
        {
            get => _useCachedData;
            set => _useCachedData = value;
        }
    }
    public class LightGBMBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public LightGBMBinaryClassificationTrainer(int _numberOfLeaves,
            int _numberOfIterations,
            int _minExampleCountPerLeaf,
            float _learningRate) :
            base(BinaryClassificationTrainer.LightGbm)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfIterations = _numberOfIterations;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
            LearningRate = _learningRate;
        }
        private int _numberOfLeaves;
        private int _numberOfIterations;
        private int _minExampleCountPerLeaf;
        private float _learningRate;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }
    public class LinearSVMBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public LinearSVMBinaryClassificationTrainer(int _numberOfIterations) :
            base(BinaryClassificationTrainer.LinearSvm)
        {
            NumberOfIterations = _numberOfIterations;
        }

        private int _numberOfIterations;    
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
    }
    public class SDCALogisticRegressionBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public SDCALogisticRegressionBinaryClassificationTrainer(float _L1,
            float _L2,
            int _maxIterations) :
            base(BinaryClassificationTrainer.SdcaLogisticRegression)
        {
            L1Regularization = _L1;
            L2Regularization = _L2;
            MaxIterations = _maxIterations;
        }

        private float _L1Regularization;
        private float _L2Regularization;
        private int _maxIterations;
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public int MaxIterations
        {
            get => _maxIterations;
            set => _maxIterations = value;
        }
    }
    public class SDCANoncalibratedBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public SDCANoncalibratedBinaryClassificationTrainer(float _L1,
            float _L2,
            int _maxIterations) :
            base(BinaryClassificationTrainer.SdcaNonCalibrated)
        {
            L1Regularization = _L1;
            L2Regularization = _L2;
            MaxIterations = _maxIterations;
        }

        private float _L1Regularization;
        private float _L2Regularization;
        private int _maxIterations;
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public int MaxIterations
        {
            get => _maxIterations;
            set => _maxIterations = value;
        }
    }
    public class SGDCalibratedBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public SGDCalibratedBinaryClassificationTrainer(float _learningRate,
            float _L2,
            int _numberOfIterations) :
            base(BinaryClassificationTrainer.SgdCalibrated)
        {
            LearningRate = _learningRate;
            L2Regularization = _L2;
            NumberOfIterations = _numberOfIterations;
        }

        private float _learningRate;
        private int _numberOfIterations;
        private float _L2Regularization;
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
    }
    public class SGDNoncalibratedBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public SGDNoncalibratedBinaryClassificationTrainer(float _learningRate,
            float _L2,
            int _numberOfIterations) :
            base(BinaryClassificationTrainer.SgdNonCalibrated)
        {
            LearningRate = _learningRate;
            L2Regularization = _L2;
            NumberOfIterations = _numberOfIterations;
        }

        private float _learningRate;
        private int _numberOfIterations;
        private float _L2Regularization;
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
    }
    public class SymbolicSGDLogisticRegressionBinaryClassificationTrainer : BinaryClassificationTrainerBase
    {
        public SymbolicSGDLogisticRegressionBinaryClassificationTrainer(int _numberOfIterations) :
            base(BinaryClassificationTrainer.SymbolicSgdLogisticRegression)
        {
            NumberOfIterations = _numberOfIterations;
        }

        private int _numberOfIterations;
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
    }
}
