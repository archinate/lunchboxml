﻿using Grasshopper.Kernel.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Classes
{
    public enum CodificationVariableType
    {
        Ordinal,
        Categorical,
        CategoricalBaseline,
        Continuous,
        Discrete,
        Count
    }

    public class clsCodifyDataItem
    {
        public List<string> Data = new List<string>();
        public string Label { get; set; } = "";
        public CodificationVariableType VariableType { get; set; } = CodificationVariableType.Categorical;
    }
}
