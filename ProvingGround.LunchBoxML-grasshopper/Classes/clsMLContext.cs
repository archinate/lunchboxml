﻿using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Classes
{
    public sealed class PG_MLContext
    {
        private static readonly MLContext instance = new MLContext(seed: 0);

        static PG_MLContext()
        {
        }

        private PG_MLContext()
        {
        }

        public static MLContext Instance
        {
            get
            {
                return instance;
            }
        }
    }
}
