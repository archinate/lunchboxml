﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML;
using Microsoft.ML.Data;

namespace ProvingGround.MachineLearning.Classes
{
    public class clsMLTextLoaderOptions : TextLoader.Options
    {
        public clsMLTextLoaderOptions() : base()
        {
           
        }
        public List<string> ColumnNames { get; set; }
    }
}
