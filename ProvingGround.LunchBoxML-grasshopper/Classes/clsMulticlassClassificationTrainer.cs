﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Trainers.LightGbm;

namespace ProvingGround.MachineLearning.Classes
{
    public enum MultiClassClassificationTrainer
    {
        LbfgsMaximumEntropy,
        LightGbm,
        NaiveBayes,
        SdcaMaximumEntropy,
        SdcaNonCalibrated,
        Count
    }

    public class MulticlassClassificationTrainerBase
    {
        private MultiClassClassificationTrainer _trainer; 
        public MultiClassClassificationTrainer Trainer    
        {
            get => _trainer;
            set => _trainer = value;
        }
        public MulticlassClassificationTrainerBase(MultiClassClassificationTrainer _trainer)
        {
            Trainer = _trainer;
        }   
    }

    public class LbfgsMaximumEntropyTrainer : MulticlassClassificationTrainerBase
    {
        public LbfgsMaximumEntropyTrainer(bool _denseOptimizer, 
            bool _enforceNonNegativity, 
            float _L1, 
            float _L2, 
            int _historySize,
            float optimizationTolerance,
            bool _showStatistics) : 
            base(MultiClassClassificationTrainer.LbfgsMaximumEntropy)
        {
            DenseOptimizer = _denseOptimizer;
            EnforceNonNegativity = _enforceNonNegativity;
            L1Regularization = _L1;
            L2Regularization = _L2;
            OptimizationTolerance = optimizationTolerance;
            ShowStatistics = _showStatistics;
            HistorySize = _historySize;
        }

        private bool _denseOptimmizer;
        private bool _enforceNonNegativity;
        private float _L1Regularization;
        private float _L2Regularization;
        private int _historySize;
        private float _optimizationTolerance;
        private bool _showStatistics;
        public bool DenseOptimizer
        {
            get => _denseOptimmizer;
            set => _denseOptimmizer = value;
        }
        public bool EnforceNonNegativity
        {
            get => _enforceNonNegativity;
            set => _enforceNonNegativity = value;
        }
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }

        public float OptimizationTolerance
        {
            get => _optimizationTolerance;
            set => _optimizationTolerance = value;
        }
        public bool ShowStatistics
        {
            get => _showStatistics;
            set => _showStatistics = value;
        }
        public int HistorySize
        {
            get => _historySize;
            set => _historySize = value;
        }
    }

    public class SdcaMaximumEntropyTrainer : MulticlassClassificationTrainerBase
    {
        public SdcaMaximumEntropyTrainer(float _convergenceTolerance,
            float _L1,
            float _L2,
            float _biasLearningRate,
            bool _shuffle) :
            base(MultiClassClassificationTrainer.SdcaMaximumEntropy)
        {
            ConvergenceTolerance = _convergenceTolerance;
            L1Regularization = _L1;
            L2Regularization = _L2;
            BiasLearningRate = _biasLearningRate;
            Shuffle = _shuffle;
        }

        private float _convergenceTolerance;
        private float _L1Regularization;
        private float _L2Regularization;
        private float _biasLearningRate;
        private bool _shuffle;

        public float ConvergenceTolerance
        {
            get => _convergenceTolerance;
            set => _convergenceTolerance = value;
        }
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public float BiasLearningRate
        {
            get => _biasLearningRate;
            set => _biasLearningRate = value;
        }
        public bool Shuffle
        {
            get => _shuffle;
            set => _shuffle = value;
        }
    }
    public class SdcaNonCalibratedTrainer : MulticlassClassificationTrainerBase
    {
        public SdcaNonCalibratedTrainer(float _convergenceTolerance,
            float _L1,
            float _L2,
            float _biasLearningRate,
            bool _shuffle) :
            base(MultiClassClassificationTrainer.SdcaNonCalibrated)
        {
            ConvergenceTolerance = _convergenceTolerance;
            L1Regularization = _L1;
            L2Regularization = _L2;
            BiasLearningRate = _biasLearningRate;
            Shuffle = _shuffle;
        }

        private float _convergenceTolerance;
        private float _L1Regularization;
        private float _L2Regularization;
        private float _biasLearningRate;
        private bool _shuffle;

        public float ConvergenceTolerance
        {
            get => _convergenceTolerance;
            set => _convergenceTolerance = value;
        }
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public float BiasLearningRate
        {
            get => _biasLearningRate;
            set => _biasLearningRate = value;
        }
        public bool Shuffle
        {
            get => _shuffle;
            set => _shuffle = value;
        }
    }

    public class NaiveBayesTrainer : MulticlassClassificationTrainerBase
    {
        public NaiveBayesTrainer():
            base(MultiClassClassificationTrainer.NaiveBayes)
        {
            
        }
    }

    public class LightGbmTrainer : MulticlassClassificationTrainerBase
    {
        
        public LightGbmTrainer(LightGbmMulticlassTrainer.Options.EvaluateMetricType _evaluationMetric,
            float _learningRate,
            float _sigmoid,
            bool _useSoftMax,
            int _maxBinsPerFeature) :
            base(MultiClassClassificationTrainer.LightGbm)
        {
            EvaluationMetric = _evaluationMetric;
            LearningRate = _learningRate;
            Sigmoid = _sigmoid;
            UseSoftMax = _useSoftMax;
            MaxBinsPerFeature = _maxBinsPerFeature;
        }

        private LightGbmMulticlassTrainer.Options.EvaluateMetricType _evaluationMetric;
        private float _learningRate;
        private float _sigmoid;
        private bool _useSoftMax;
        private int _maxBinsPerFeature;

        public LightGbmMulticlassTrainer.Options.EvaluateMetricType EvaluationMetric
        {
            get => _evaluationMetric;
            set => _evaluationMetric = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
        public float Sigmoid
        {
            get => _sigmoid;
            set => _sigmoid = value;
        }
        public bool UseSoftMax
        {
            get => _useSoftMax;
            set => _useSoftMax = value;
        }
        public int MaxBinsPerFeature
        {
            get => _maxBinsPerFeature;
            set => _maxBinsPerFeature = value;
        }
    }
}
