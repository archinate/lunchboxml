﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProvingGround.MachineLearning.Classes
{
    public enum RegressionTrainer
    {
        FastForest,
        FastTree,
        FastTreeTweedie,
        GAM,
        LbfgsPoissonRegression,
        LightGBM,
        OLS,
        OnlineGradientDescent,
        SDCA,
        Count
    }

    public class RegressionTrainerBase
    {
        private RegressionTrainer _trainer;
        public RegressionTrainer Trainer
        {
            get => _trainer;
            set => _trainer = value;
        }
        public RegressionTrainerBase(RegressionTrainer _trainer)
        {
            Trainer = _trainer;
        }
    }

    public class FastForestRegressionTrainer : RegressionTrainerBase
    {
        public FastForestRegressionTrainer(int _numberOfLeaves,
            int _numberOfTrees,
            int _minExampleCountPerLeaf) :
            base(RegressionTrainer.FastForest)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfTrees = _numberOfTrees;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
        }

        private int _numberOfLeaves;
        private int _numberOfTrees;
        private int _minExampleCountPerLeaf;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfTrees
        {
            get => _numberOfTrees;
            set => _numberOfTrees = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
    }

    public class FastTreeRegressionTrainer : RegressionTrainerBase
    {
        public FastTreeRegressionTrainer(int _numberOfLeaves,
            int _numberOfTrees,
            int _minExampleCountPerLeaf,
            float _learningRate) :
            base(RegressionTrainer.FastTree)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfTrees = _numberOfTrees;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
            LearningRate = _learningRate;
        }

        private int _numberOfLeaves;
        private int _numberOfTrees;
        private int _minExampleCountPerLeaf;
        private float _learningRate;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfTrees
        {
            get => _numberOfTrees;
            set => _numberOfTrees = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }
    public class FastTreeTweedieRegressionTrainer : RegressionTrainerBase
    {
        public FastTreeTweedieRegressionTrainer(int _numberOfLeaves,
            int _numberOfTrees,
            int _minExampleCountPerLeaf,
            float _learningRate) :
            base(RegressionTrainer.FastTreeTweedie)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfTrees = _numberOfTrees;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
            LearningRate = _learningRate;
        }

        private int _numberOfLeaves;
        private int _numberOfTrees;
        private int _minExampleCountPerLeaf;
        private float _learningRate;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfTrees
        {
            get => _numberOfTrees;
            set => _numberOfTrees = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }

    public class GAMRegressionTrainer : RegressionTrainerBase
    {
        public GAMRegressionTrainer(int _numberOfIterations,
            int _maxBinCountPerFeature,
            float _learningRate) :
            base(RegressionTrainer.GAM)
        {
            NumberOfIterations = _numberOfIterations;
            MaxBinCountPerFeature = _maxBinCountPerFeature;
            LearningRate = _learningRate;
        }

        private int _numberOfIterations;
        private int _maxBinCountPerFeature;
        private float _learningRate;
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public int MaxBinCountPerFeature
        {
            get => _maxBinCountPerFeature;
            set => _maxBinCountPerFeature = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }
    public class LbfgsPoissonRegressionTrainer : RegressionTrainerBase
    {
        public LbfgsPoissonRegressionTrainer(bool _enforceNonNegativity,
            float _L1,
            float _L2,
            int _historySize,
            float optimizationTolerance) :
            base(RegressionTrainer.LbfgsPoissonRegression)
        {
            EnforceNonNegativity = _enforceNonNegativity;
            L1Regularization = _L1;
            L2Regularization = _L2;
            OptimizationTolerance = optimizationTolerance;
            HistorySize = _historySize;
        }

        private bool _enforceNonNegativity;
        private float _L1Regularization;
        private float _L2Regularization;
        private int _historySize;
        private float _optimizationTolerance;
        public bool EnforceNonNegativity
        {
            get => _enforceNonNegativity;
            set => _enforceNonNegativity = value;
        }
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }

        public float OptimizationTolerance
        {
            get => _optimizationTolerance;
            set => _optimizationTolerance = value;
        }
        public int HistorySize
        {
            get => _historySize;
            set => _historySize = value;
        }
    }

    public class LightGBMRegressionTrainer : RegressionTrainerBase
    {
        public LightGBMRegressionTrainer(int _numberOfLeaves,
            int _numberOfIterations,
            int _minExampleCountPerLeaf,
            float _learningRate) :
            base(RegressionTrainer.LightGBM)
        {
            NumberOfLeaves = _numberOfLeaves;
            NumberOfIterations = _numberOfIterations;
            MinExampleCountPerLeaf = _minExampleCountPerLeaf;
            LearningRate = _learningRate;
        }

        private int _numberOfLeaves;
        private int _numberOfIterations;
        private int _minExampleCountPerLeaf;
        private float _learningRate;

        public int NumberOfLeaves
        {
            get => _numberOfLeaves;
            set => _numberOfLeaves = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
        public int MinExampleCountPerLeaf
        {
            get => _minExampleCountPerLeaf;
            set => _minExampleCountPerLeaf = value;
        }
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
    }

    public class OLSRegressionTrainer : RegressionTrainerBase
    {
        public OLSRegressionTrainer() :
            base(RegressionTrainer.OLS)
        {

        }
    }
    public class OnlineGradientDescentRegressionTrainer : RegressionTrainerBase
    {
        public OnlineGradientDescentRegressionTrainer(float _learningRate,
            bool _decreaseLearningRate,
            float _L2,
            int _numberOfIterations) :
            base(RegressionTrainer.OnlineGradientDescent)
        {
            LearningRate = _learningRate;
            DecreaseLearningRate = _decreaseLearningRate;
            L2Regularization = _L2;
            NumberOfIterations = _numberOfIterations;
        }
        private float _learningRate;
        private bool _decreaseLearningRate;
        private float _L2Regularization;
        private int _numberOfIterations;
        public float LearningRate
        {
            get => _learningRate;
            set => _learningRate = value;
        }
        public bool DecreaseLearningRate
        {
            get => _decreaseLearningRate;
            set => _decreaseLearningRate = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
    }
    public class SDCARegressionTrainer : RegressionTrainerBase
    {
        public SDCARegressionTrainer(float _L1,
            float _L2,
            int _numberOfIterations) :
            base(RegressionTrainer.SDCA)
        {
            L1Regularization = _L1;
            L2Regularization = _L2;
            NumberOfIterations = _numberOfIterations;
        }

        private float _L1Regularization;
        private float _L2Regularization;
        private int _numberOfIterations;
        public float L1Regularization
        {
            get => _L1Regularization;
            set => _L1Regularization = value;
        }
        public float L2Regularization
        {
            get => _L2Regularization;
            set => _L2Regularization = value;
        }
        public int NumberOfIterations
        {
            get => _numberOfIterations;
            set => _numberOfIterations = value;
        }
    }
}
