﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using ProvingGround.MachineLearning.Parsers;

namespace ProvingGround.MachineLearning.Classes
{
    class TinyYolo2ModelScorer
    {
        private readonly string imagePath;
        private readonly string modelLocation;
        private readonly MLContext mlContext;
        public string outputMessage = "";

        private IList<TinyYolo2BoundingBox> _boundingBoxes = new List<TinyYolo2BoundingBox>();

        public TinyYolo2ModelScorer(string imagePath, string modelLocation, MLContext mlContext)
        {
            this.imagePath = imagePath;
            this.modelLocation = modelLocation;
            this.mlContext = mlContext;
            this.outputMessage = "";
        }

        public struct ImageNetSettings
        {
            public const int imageHeight = 416;
            public const int imageWidth = 416;
        }

        public struct TinyYolo2ModelSettings
        {
            // for checking Tiny yolo2 Model input and  output  parameter names,
            //you can use tools like Netron, 
            // which is installed by Visual Studio AI Tools

            // input tensor name
            public const string ModelInput = "image";

            // output tensor name
            public const string ModelOutput = "grid";
        }

        private ITransformer LoadModel(string modelLocation)
        {
            this.outputMessage += "Read model\n";
            this.outputMessage += $"Model location: {modelLocation}\n";
            this.outputMessage += $"Default parameters: image size=({ImageNetSettings.imageWidth},{ImageNetSettings.imageHeight})\n";

            // Create IDataView from empty list to obtain input data schema
            var data = mlContext.Data.LoadFromEnumerable(new List<ImageNetData>());

            // Define scoring pipeline
            var pipeline = mlContext.Transforms.LoadImages(outputColumnName: "image", imageFolder: "", inputColumnName: nameof(ImageNetData.ImagePath))
                            .Append(mlContext.Transforms.ResizeImages(outputColumnName: "image", imageWidth: ImageNetSettings.imageWidth, imageHeight: ImageNetSettings.imageHeight, inputColumnName: "image"))
                            .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "image"))
                            .Append(mlContext.Transforms.ApplyOnnxModel(modelFile: modelLocation, outputColumnNames: new[] { TinyYolo2ModelSettings.ModelOutput }, inputColumnNames: new[] { TinyYolo2ModelSettings.ModelInput }));

            // Fit scoring pipeline
            var model = pipeline.Fit(data);

            return model;
        }

        private IEnumerable<float[]> PredictDataUsingModel(IDataView testData, ITransformer model)
        {
            this.outputMessage += $"Image location: {imagePath}\n";
            this.outputMessage += "\n";
            this.outputMessage += "=====Identify the objects in the image=====\n";
            this.outputMessage += "\n";

            IDataView scoredData = model.Transform(testData);

            IEnumerable<float[]> probabilities = scoredData.GetColumn<float[]>(TinyYolo2ModelSettings.ModelOutput);

            return probabilities;
        }

        public IEnumerable<float[]> Score(IDataView data)
        {
            var model = LoadModel(modelLocation);

            return PredictDataUsingModel(data, model);
        }
    }
}
