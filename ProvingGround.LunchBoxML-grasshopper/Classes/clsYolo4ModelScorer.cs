﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using ProvingGround.MachineLearning.Classes.DataStructures;
using ProvingGround.MachineLearning.Parsers;
using static Microsoft.ML.Transforms.Image.ImageResizingEstimator;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ProvingGround.MachineLearning.Classes
{
    class Yolo4ModelScorer
    {
        private readonly string imagePath;
        private readonly string modelLocation;
        private readonly MLContext mlContext;
        public string outputMessage = "";

        private string[] classNames = new string[]
        {
            "person", "bicycle", "car", "motorbike", "aeroplane",
            "bus", "train", "truck", "boat", "traffic light",
            "fire hydrant", "stop sign", "parking meter", "bench",
            "bird", "cat", "dog", "horse", "sheep", "cow", "elephant",
            "bear", "zebra", "giraffe", "backpack", "umbrella",
            "handbag", "tie", "suitcase", "frisbee", "skis",
            "snowboard", "sports ball", "kite", "baseball bat",
            "baseball glove", "skateboard", "surfboard", "tennis racket",
            "bottle", "wine glass", "cup", "fork", "knife", "spoon",
            "bowl", "banana", "apple", "sandwich", "orange", "broccoli",
            "carrot", "hot dog", "pizza", "donut", "cake", "chair",
            "sofa", "pottedplant", "bed", "diningtable", "toilet",
            "tvmonitor", "laptop", "mouse", "remote", "keyboard", "cell phone",
            "microwave", "oven", "toaster", "sink", "refrigerator", "book",
            "clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush"
        };

        private IList<YoloV4Result> _boundingBoxes = new List<YoloV4Result>();

        public Yolo4ModelScorer(string imagePath, string modelLocation, MLContext mlContext)
        {
            this.imagePath = imagePath;
            this.modelLocation = modelLocation;
            this.mlContext = mlContext;
            this.outputMessage = "";
        }

        public struct BitmapSettings
        {
            public const int imageHeight = 416;
            public const int imageWidth = 416;
        }

        private ITransformer LoadModel(string modelLocation)
        {
            this.outputMessage += "Read model\n";
            this.outputMessage += $"Model location: {modelLocation}\n";
            this.outputMessage += $"Default parameters: image size=({BitmapSettings.imageWidth},{BitmapSettings.imageHeight})\n";

            // Create IDataView from empty list to obtain input data schema
            var data = mlContext.Data.LoadFromEnumerable(new List<YoloV4BitmapData>());

            // Define scoring pipeline
            var pipeline = mlContext.Transforms.ResizeImages(inputColumnName: "bitmap", outputColumnName: "input_1:0", imageWidth: BitmapSettings.imageWidth, imageHeight: BitmapSettings.imageHeight, resizing: ResizingKind.IsoPad)
                .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "input_1:0", scaleImage: 1f / 255f, interleavePixelColors: true))
                .Append(mlContext.Transforms.ApplyOnnxModel(
                    shapeDictionary: new Dictionary<string, int[]>()
                    {
                        { "input_1:0", new[] { 1, 416, 416, 3 } },
                        { "Identity:0", new[] { 1, 52, 52, 3, 85 } },
                        { "Identity_1:0", new[] { 1, 26, 26, 3, 85 } },
                        { "Identity_2:0", new[] { 1, 13, 13, 3, 85 } },
                    },
                    inputColumnNames: new[]
                    {
                        "input_1:0"
                    },
                    outputColumnNames: new[]
                    {
                        "Identity:0",
                        "Identity_1:0",
                        "Identity_2:0"
                    },
                    modelFile: modelLocation, recursionLimit: 100));

            // Fit scoring pipeline
            var model = pipeline.Fit(data);

            return model;
        }

        private IReadOnlyList<YoloV4Result> PredictDataUsingModel(Bitmap bitmap, ITransformer model)
        {
            this.outputMessage += $"Image location: {imagePath}\n";
            this.outputMessage += "\n";
            this.outputMessage += "=====Identify the objects in the image=====\n";
            this.outputMessage += "\n";

            // Create prediction engine
            var predictionEngine = mlContext.Model.CreatePredictionEngine<YoloV4BitmapData, YoloV4Prediction>(model);
            var predict = predictionEngine.Predict(new YoloV4BitmapData() { Image = bitmap });
            var results = predict.GetResults(classNames, 0.3f, 0.7f);

            return results;
        }

        public IReadOnlyList<YoloV4Result> Score()
        {
            var model = LoadModel(modelLocation);
            var bitmap = new Bitmap(Image.FromFile(imagePath));
            return PredictDataUsingModel(bitmap, model);
        }

        
    }
}
