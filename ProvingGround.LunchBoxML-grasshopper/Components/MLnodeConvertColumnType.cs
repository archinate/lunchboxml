﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Windows.Forms;
using GH_IO.Serialization;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Convert Column Type
    /// </summary>
    public class MLnodeConvertColumnType : GH_Component
    {
        public enum ConvertDataType
        {
            Number,
            Boolean,
            Text,
            DateTime,
            Key,
            Count
        }

        public class ConvertDataType_Param : Grasshopper.Kernel.Parameters.Param_Integer
        {
            public override GH_Exposure Exposure => GH_Exposure.hidden;

            public override Guid ComponentGuid
            {
                get { return new Guid("27abefb4-db6f-4c40-abe3-82b693374f03"); }
            }

            public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
            {
                MLnodeConvertColumnType parent = this.Attributes.Parent.DocObject as MLnodeConvertColumnType;

                Menu_AppendSeparator(menu);
                MenuHeaderItem mhi = new MenuHeaderItem();
                mhi.Text = "Data Type";
                mhi.Font = GH_FontServer.StandardItalic;
                if (!parent.ShowMenu) mhi.Enabled = false;
                else mhi.Enabled = true;
                menu.Items.Add(mhi);

                for (int i = 0; i < (int)ConvertDataType.Count; i++)
                {
                    Menu_AppendItem(menu, ((ConvertDataType)i).ToString(), MenuItem_Clicked, parent.ShowMenu, parent.ShowMenu && (ConvertDataType == ((ConvertDataType)i))).Tag = (ConvertDataType)i;
                }
            }

            private void MenuItem_Clicked(object sender, EventArgs e)
            {
                if (sender is ToolStripMenuItem)
                {
                    ToolStripMenuItem ts = sender as ToolStripMenuItem;
                    ConvertDataType = (ConvertDataType)ts.Tag;
                    MLnodeConvertColumnType parent = this.Attributes.Parent.DocObject as MLnodeConvertColumnType;
                    parent.ConvertInt = (int)ConvertDataType;
                    this.PersistentData[0][0].Value = (int)ConvertDataType;
                    ExpireSolution(true);
                }
            }

            public override bool Write(GH_IWriter writer)
            {
                writer.SetInt32("m_convert", (int)ConvertDataType);
                return base.Write(writer);
            }
            public override bool Read(GH_IReader reader)
            {
                int cmode = 0;
                if (reader.TryGetInt32("m_convert", ref cmode))
                {
                    ConvertDataType = (ConvertDataType)cmode;
                }
                return base.Read(reader);
            }

            public ConvertDataType ConvertDataType
            {
                get { return m_convert; }
                set { m_convert = value; }
            }

            private ConvertDataType m_convert = ConvertDataType.Number;
        }

        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeConvertColumnType()
            : base("Convert Column Type", "Convert", "Convert a column to a new data type. This transform operates over numeric, boolean, text, DateTime and key data types.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("2a5b6ba0-439c-49b7-83e7-41554ad48fe1"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_ConvertColumnType; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to change.", GH_ParamAccess.item);
            pManager.AddTextParameter("Column", "Column", "The name of the column in the data set to convert.", GH_ParamAccess.item);

            ConvertDataType_Param convertParam = new ConvertDataType_Param();
            convertParam.ConvertDataType = ConvertDataType.Number;
            ConvertInt = (int)convertParam.ConvertDataType;
            convertParam.SetPersistentData(ConvertInt);
            pManager.AddParameter(convertParam, "Convert To Type", "Convert To", "Convert To Type: \n" +
                " (0) Number\n" +
                " (1) Boolean\n" +
                " (2) Text\n" +
                " (3) Date Time\n" +
                " (4) Key\n" , GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }
        #endregion

        public int ConvertInt = 0;
        public bool ShowMenu = true;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            string columnName = "";
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<string>(1, ref columnName)
                )
            {
                ConvertDataType_Param convertParam = this.Params.Input[2] as ConvertDataType_Param;
                ConvertDataType dataType = convertParam.ConvertDataType;
                ConvertInt = (int)dataType;

                if (convertParam.SourceCount > 0)
                {
                    DA.GetData<int>(2, ref ConvertInt);
                    if (ConvertInt >= (int)ConvertDataType.Count) ConvertInt = (int)ConvertDataType.Count - 1;
                    else if (ConvertInt <= 0) ConvertInt = 0;

                    dataType = (ConvertDataType)ConvertInt;
                    ShowMenu = false;
                }
                else
                {
                    convertParam.PersistentData[0][0].Value = ConvertInt;
                    ShowMenu = true;
                }

                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;

                DataKind dType = DataKind.Single;
                if (dataType == ConvertDataType.Boolean) dType = DataKind.Boolean;
                else if (dataType == ConvertDataType.Text) dType = DataKind.String;
                else if (dataType == ConvertDataType.DateTime) dType = DataKind.DateTime;
                else if (dataType == ConvertDataType.Key) dType = DataKind.UInt32;
                else dType = DataKind.Single;

                var pipeline = mlContext.Transforms.Conversion.ConvertType(new[]
                {
                    new InputOutputColumnPair(columnName + "_converted", columnName)
                }, dType);

                var transformer = pipeline.Fit(data);
                IDataView transformedData = transformer.Transform(data);

                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



