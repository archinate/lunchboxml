﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML.Data;
using Microsoft.ML;
using Grasshopper.Kernel.Data;
using Grasshopper;
using System.Reflection;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeCreateDataSet : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeCreateDataSet()
            : base("Create Data Set", "Create", "Create a data set which can be use for training a machine learning algorithm.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("cc754e40-f9aa-4bda-8ea2-0e5af7702c51"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_CreateDataSet; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Labels", "Labels", "Labels used to name each column of data.", GH_ParamAccess.list);
            pManager.AddGenericParameter("Data", "Data", "The data.", GH_ParamAccess.tree);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The codified data.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<string> labels = new List<string>();
            GH_Structure<IGH_Goo> data = new GH_Structure<IGH_Goo>();
            DataTree<object> dataConverted = new DataTree<object>();
            if (
                    DA.GetDataList<string>(0, labels) &&
                    DA.GetDataTree<IGH_Goo>(1, out data)
               )
            {
                var i_list = data.Branches[0];

                if (labels.Count != i_list.Count)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "The number of labels does not match the input data structure.");
                    return;
                }

                var mlContext = PG_MLContext.Instance;

                Dictionary<string, Type> dataTypes = new Dictionary<string, Type>();

                for(int i = 0; i < i_list.Count; i++)
                {
                    var obj = i_list[i];
                    string label = labels[i];
                    if (obj != null)
                    {
                        Type t = obj.GetType();
        
                        if (t == typeof(GH_Boolean))
                        {
                            dataTypes.Add(label, typeof(bool));
                        }
                        else if (t == typeof(GH_Number))
                        {
                            dataTypes.Add(label, typeof(float));
                        }
                        else if (t == typeof(GH_Integer))
                        {
                            dataTypes.Add(label, typeof(int));
                        }
                        else if (t == typeof(GH_Time))
                        {
                            dataTypes.Add(label, typeof(DateTime));
                        }
                        else
                        {
                            dataTypes.Add(label, typeof(string));
                        }
                    }    
                }


                var properties = new List<DynamicTypeProperty>();

                foreach (KeyValuePair<string, Type> entry in dataTypes)
                {
                    properties.Add(new DynamicTypeProperty(entry.Key, entry.Value));
                }

                // create the new type
                var dynamicType = DynamicType.CreateDynamicType(properties);
                var schema = SchemaDefinition.Create(dynamicType);
                var dynamicList = DynamicType.CreateDynamicList(dynamicType);

                // get an action that will add to the list
                var addAction = DynamicType.GetAddAction(dynamicList);

                //convert the generic data tree to data tree with type conversion
                for (int pi = 0; pi < data.PathCount; pi++)
                {
                    var path = data.Paths[pi];
                    var list = data.Branches[pi];
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i] != null)
                        {
                            var obj = list[i];
                            if (obj != null)
                            {
                                Type t = obj.GetType();

                                if (t == typeof(GH_Boolean))
                                {
                                    GH_Boolean n = obj as GH_Boolean;
                                    dataConverted.Add(n.Value, path);
                                }
                                else if (t == typeof(GH_Number))
                                {
                                    GH_Number n = obj as GH_Number;
                                    dataConverted.Add((float)n.Value, path);
                                }
                                else if (t == typeof(GH_Integer))
                                {
                                    GH_Integer n = obj as GH_Integer;
                                    dataConverted.Add(n.Value, path);
                                }
                                else if (t == typeof(GH_Time))
                                {
                                    GH_Time n = obj as GH_Time;
                                    dataConverted.Add(n.Value, path);
                                }
                                else
                                {
                                    GH_String n = obj as GH_String;
                                    dataConverted.Add(n.Value, path);
                                }
                            }
                        }                          
                    }
                }

                // call the action, with an object[] containing parameters in exact order added
                for (int pi = 0; pi < dataConverted.BranchCount; pi++)
                {
                    var list = dataConverted.Branches[pi];
                    object[] dataObj = new object[list.Count];
                    for (int i = 0; i < list.Count; i++)
                    {
                        dataObj[i] = list[i];
                    }
                    addAction.Invoke(dataObj);
                }

                var dataType = mlContext.Data.GetType();
                var loadMethodGeneric = dataType.GetMethods().First(method => method.Name == "LoadFromEnumerable" && method.IsGenericMethod);
                var loadMethod = loadMethodGeneric.MakeGenericMethod(dynamicType);
                var trainData = (IDataView)loadMethod.Invoke(mlContext.Data, new[] { dynamicList, schema });

                DA.SetData(0, trainData);
            }

        }
        #endregion
    }
}



