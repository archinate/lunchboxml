﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeDataLoaderOptions : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeDataLoaderOptions()
            : base("Text Loader Options", "Options", "Additional options to control how a data file is loaded.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("deaac45c-8a40-40f8-b451-89e0635c52f5"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_TextLoaderOptions; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Columns", "Columns", "Specifies the input column names that should be mapped to the data set columns. If column names are provided, they will override the header input.", GH_ParamAccess.list);
            pManager[0].Optional = true;
            pManager.AddBooleanParameter("Trim Whitespace", "Trim Whitespace", "Wheter to remove trailing whitespace from lines.", GH_ParamAccess.item, false);
            pManager.AddBooleanParameter("Allow Quoting", "Allow Quoting", "Whether the input may include double-quoted values. This parameter is used to distinguish separator characters in an input value from actual separators. When true, separators within double quotes are treated as part of the input value. When false, all separators, even those within quotes, are treated as delimiting a new column.", GH_ParamAccess.item, false);
           
              
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Text Loader Options", "Options", "Text Loader Options.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            clsMLTextLoaderOptions options = new clsMLTextLoaderOptions();
            
            List<string> columnNames = new List<string>();
            bool trimWhitespace = false;
            bool allowQuoting = false;


            if (DA.GetDataList<string>(0, columnNames))
            {
                options.ColumnNames = columnNames;
            }
            if (DA.GetData<bool>(1, ref trimWhitespace))
            {
                options.TrimWhitespace = trimWhitespace;
            }
            if (DA.GetData<bool>(2, ref allowQuoting))
            {
                options.AllowQuoting = allowQuoting;
            }

            DA.SetData(0, options);  
        }
        #endregion
    }
}



