﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using static Microsoft.ML.DataOperationsCatalog;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeFeaturizeText : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeFeaturizeText()
            : base("Featurize Text", "Featurize", "Transforms a text column into a featurized vector of numbers that represents normalized counts of n-grams and char-grams.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("b980959c-0315-4edc-87dc-5d92a3d63677"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_FeaturizeText; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to modify.", GH_ParamAccess.item);
            pManager.AddTextParameter("Column", "Column", "The name of the text column in the data set to featurize.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            string columnName = "";
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<string>(1, ref columnName)
               )
            {
                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;

                var pipeline = mlContext.Transforms.Text.FeaturizeText(columnName + "_featurized", columnName);
                var transformer = pipeline.Fit(data);
                IDataView transformedData = transformer.Transform(data);

                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



