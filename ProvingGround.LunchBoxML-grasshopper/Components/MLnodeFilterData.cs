﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeFilterData: GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeFilterData()
            : base("Filter Data", "Filter Data", "Filter data based on the values of a column.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("b2481595-e41e-4d87-ac1d-9c9a8ef13302"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_FilterData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data to Filter", "Data", "The data to filter", GH_ParamAccess.item);
            pManager.AddTextParameter("Column", "Column", "The name of the column in the data set to filter.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Lower Bound", "Lower Bound", "The inclusive lower bound for the filtered data.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Upper Bound", "Upper Bound", "The exclusive upper bound for the filtered data.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The filtered data set.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            string columnName = "";
            double lowerBnd = 0.0;
            double upperBnd = 1.0;
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<string>(1, ref columnName) &&
                    DA.GetData<double>(2, ref lowerBnd) &&
                    DA.GetData<double>(3, ref upperBnd)
               )
            {
                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;
                IDataView transformedData = mlContext.Data.FilterRowsByColumn(data, columnName, lowerBnd, upperBnd);
                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



