﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Parameters;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeGetColumnNames : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeGetColumnNames()
            : base("Get Column Label", "Column Label", "Get the names of the columns in a data set.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("c0431065-bc87-410c-8068-e768a5ebba5d"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_GetColumnNames; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to change.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            //no outputs as these will be added dynamically based on data set configuration
        }

        public void UpdateParamNames()
        {
            GH_ComponentParamServer.IGH_SyncObject so = Params.EmitSyncObject();

            // Remove superfluous outputs.
            while (columnNames.Length < Params.Output.Count)
            {
                Params.UnregisterOutputParameter(Params.Output[Params.Output.Count - 1], true);
            }

            // Add required output.
            while (columnNames.Length > Params.Output.Count)
            {
                IGH_Param param = new Param_String();
                Params.RegisterOutputParam(param);
            }

            for (int i = 0; i < Params.Output.Count; i++)
            {
                var param = Params.Output[i] as Param_String;
                param.MutableNickName = false;
                param.Name = columnNames[i];
                param.NickName = columnNames[i];
                param.Access = GH_ParamAccess.item;
            }

            Params.Sync(so);
            this.OnAttributesChanged();
        }

        private void SolutionCallback(GH_Document doc)
        {
            UpdateParamNames();
            ExpireSolution(false);
        }

        public override bool Write(GH_IO.Serialization.GH_IWriter writer)
        {
            string vars = "";
            for (int i = 0; i < columnNames.Length; i++)
            {
                vars += columnNames[i];
                if (i < columnNames.Length - 1) vars += ",";
            }
            writer.SetString("columns", vars);
            return base.Write(writer);
        }

        public override bool Read(GH_IO.Serialization.GH_IReader reader)
        {
            string vars = reader.GetString("columns");

            var elements = vars.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);

            columnNames = new string[elements.Length];
            int i = 0;
            foreach (string item in elements)
            {
                columnNames[i] = item;
                i++;
            }

            UpdateParamNames();
            return base.Read(reader);
        }
        #endregion

        //private bool refresh;
        private string[] p_columnNames = new string[] { };
        private string[] columnNames = new string[] { };

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data)
                )
            {
                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;
                var schema = data.Schema;

                columnNames = new string[schema.Count];

                int i = 0;
                foreach (var column in schema)
                {
                    columnNames[i] = column.Name;
                    i++;
                }

                if (!cls_Utilities.CompareArrays(columnNames, p_columnNames))
                {
                    // This component needs to be adjusted.
                    // Do not solve it now, instead schedule a callback.
                    p_columnNames = columnNames;
                    DA.DisableGapLogic();
                    OnPingDocument().ScheduleSolution(1, SolutionCallback);
                    return;
                }

                for (int n = 0; n < columnNames.Length; n++)
                {
                    DA.SetData(n, columnNames[n]);
                }

                p_columnNames = columnNames;
            }
        }
        #endregion
    }
}



