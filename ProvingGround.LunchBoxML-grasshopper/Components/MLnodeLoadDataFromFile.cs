﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeLoadDataFromFile : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeLoadDataFromFile()
            : base("Load Data From Text File", "Load Data", "Load a data set from a file.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("1cb3e240-ef15-4b6b-bd96-304d9bbeee7c"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LoadDataFromTextFile; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("File Path", "File Path", "File path to a file.", GH_ParamAccess.item);
            pManager.AddTextParameter("Separator Character", "Separator", "The character used to separate data points in a row.", GH_ParamAccess.item, ",");
            pManager.AddBooleanParameter("Has Header", "Has Header", "Does this file contain an inital line containing feature names. When true, the loader will skip the first line when the Load method is called.", GH_ParamAccess.item, true);
            pManager.AddGenericParameter("Data Loader Options", "Options", "Additional options to control how a data file is loaded.", GH_ParamAccess.item);
            pManager[3].Optional = true;
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The loaded data set.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_String path = new GH_String();
            string separatorChar = "";
            bool hasHeader = true;
            GH_ObjectWrapper _options = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_String>(0, ref path) &&
                    DA.GetData<string>(1, ref separatorChar) &&
                    DA.GetData<bool>(2, ref hasHeader)
               )
            {
                var mlContext = PG_MLContext.Instance;
                clsMLTextLoaderOptions options = new clsMLTextLoaderOptions();

                if (this.Params.Input[3].SourceCount > 0)
                {
                    DA.GetData<GH_ObjectWrapper>(3, ref _options);
                    options = _options.Value as clsMLTextLoaderOptions;
                }

                options.HasHeader = hasHeader;
                options.Separators = new char[] { char.Parse(separatorChar) };

                if (options.ColumnNames != null && options.ColumnNames.Count > 0)
                {
                    string items = "";
                    if (options.HasHeader)
                        items = File.ReadLines(path.Value.ToString()).ElementAt(2);
                    else items = File.ReadLines(path.Value.ToString()).First();
                    var columns = PopulateColumns(options, items, options.ColumnNames.ToArray());

                    IDataView data = mlContext.Data.LoadFromTextFile(path.Value.ToString(), columns,
                        separatorChar: options.Separators[0],
                        hasHeader: options.HasHeader,
                        allowQuoting: options.AllowQuoting,
                        trimWhitespace: options.TrimWhitespace);
                    DA.SetData(0, data);
                }
                else if (options.HasHeader)
                {
                    string header = File.ReadLines(path.Value.ToString()).First();
                    string items = File.ReadLines(path.Value.ToString()).ElementAt(2);
                    string[] columnNames = header.Split(options.Separators[0]);
                    var columns = PopulateColumns(options, items, columnNames);

                    IDataView data = mlContext.Data.LoadFromTextFile(path.Value.ToString(), columns,
                        separatorChar: options.Separators[0],
                        hasHeader: options.HasHeader,
                        allowQuoting: options.AllowQuoting,
                        trimWhitespace: options.TrimWhitespace);
                    DA.SetData(0, data);
                }
                else
                {
                    IDataView data = mlContext.Data.LoadFromTextFile(path.Value.ToString());
                    DA.SetData(0, data);
                }
            }
        }

        private TextLoader.Column[] PopulateColumns(clsMLTextLoaderOptions options, string items, string[] columnNames)
        {
            TextLoader.Column[] columns = new TextLoader.Column[columnNames.Length];

            string[] values = items.Split(options.Separators[0]);

            double dr; float fr; int ir; bool br; DateTime tr;

            for (int i = 0; i < columnNames.Length; i++)
            {
                DataKind type;
                if (Double.TryParse(values[i], out dr)) type = DataKind.Single;
                else if (Single.TryParse(values[i], out fr)) type = DataKind.Single;
                else if (Int32.TryParse(values[i], out ir)) type = DataKind.UInt32;
                else if (bool.TryParse(values[i], out br)) type = DataKind.Boolean;
                else if (DateTime.TryParse(values[i], out tr)) type = DataKind.DateTime;
                else type = DataKind.String;

                columns[i] = new TextLoader.Column(columnNames[i], type, i);
            }
            return columns;
        }
        #endregion
    }
}

