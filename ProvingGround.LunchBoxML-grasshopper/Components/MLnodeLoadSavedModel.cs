﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;


using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Accord;
using System.Windows.Navigation;
using GH_IO.Serialization;
using System.Runtime.CompilerServices;
using Microsoft.ML;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    ///  Load A Saved Model
    /// </summary>
    public class MLnodeLoadSavedModel : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeLoadSavedModel()
            : base("Load Saved Model", "Load Model", "Deserialize a model from a saved file (*.zip)", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("e04d0d4e-8228-412d-9092-2e378c301964"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LoadModelFromFile; }
        }
        #endregion

        #region Inputs/Outputs

        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("File Path", "File Path", "File path to saved model (*.zip).", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Loaded Model", "Model", "Loaded Model", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_String path = new GH_String();

            if (
                    DA.GetData<GH_String>(0, ref path)
               )
            {
                try
                {
                    string fullpath = path.Value;
                    var mlContext = PG_MLContext.Instance;
                    DataViewSchema modelSchema;
                    ITransformer trainedModel;
                    try
                    {
                        trainedModel = mlContext.Model.Load(fullpath, out modelSchema);
                        DA.SetData(0, trainedModel);
                        return;
                    }
                    catch (InvalidCastException e) { }
                }
                catch (InvalidCastException e)
                {
                    this.Message = "Incorrect Type";
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Incorrect model type. Please try changing the model type input to match the model being deserialized.");
                }
            }
        }
        #endregion
    }
}



