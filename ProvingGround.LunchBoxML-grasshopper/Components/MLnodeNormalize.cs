﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Windows.Forms;
using GH_IO.Serialization;
using Grasshopper.Kernel.Parameters;
using Microsoft.ML.Transforms;

namespace ProvingGround.MachineLearning
{
    public enum NormalizeMode
    {
        MinMax,
        MeanVariance,
        LogMeanVariance,
        NormalizeBinning,
        Count
    }

    public class NormalizeMode_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("f1208e9b-c7c2-4b78-8fe3-3d3983a39700"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            MLnodeNormalize parent = this.Attributes.Parent.DocObject as MLnodeNormalize;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Normalization Mode";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowNormalizeModeMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)NormalizeMode.Count; i++)
            {
                Menu_AppendItem(menu, ((NormalizeMode)i).ToString(), MenuItem_Clicked, parent.ShowNormalizeModeMenu, parent.ShowNormalizeModeMenu && (NormalizeMode == ((NormalizeMode)i))).Tag = (NormalizeMode)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                NormalizeMode = (NormalizeMode)ts.Tag;
                MLnodeNormalize parent = this.Attributes.Parent.DocObject as MLnodeNormalize;
                parent.NormalizeModeInt = (int)NormalizeMode;
                this.PersistentData[0][0].Value = (int)NormalizeMode;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_normalizeMode", (int)NormalizeMode);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int nmode = 0;
            if (reader.TryGetInt32("m_normalizeMode", ref nmode))
            {
                NormalizeMode = (NormalizeMode)nmode;
            }
            return base.Read(reader);
        }

        public NormalizeMode NormalizeMode
        {
            get { return m_normalizeMode; }
            set { m_normalizeMode = value; }
        }

        private NormalizeMode m_normalizeMode = NormalizeMode.MinMax;
    }

    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeNormalize: GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeNormalize()
            : base("Normalize Data", "Normalize", "Normalizes the values of a column in a data set.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6b9113fc-c2e9-489e-93f9-27b26e29bd71"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NormalizeData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to change.", GH_ParamAccess.item);
            pManager.AddTextParameter("Columns", "Columns", "The names of the columns in the data set to normalize.", GH_ParamAccess.list);
            pManager.AddBooleanParameter("Fix Zero", "Fix Zero", "Whether to map zero to zero, preserving sparsity. When fixZero is false, the normalized interval is [0,1] and the distribution of the normalized values depends on the normalization mode. When fixZero is set to true, the normalized interval is [−1,1] with the distribution of the normalized values depending on the normalization mode, but the behavior is different. With this method, the distribution depends on how far away the number is from 0, resulting in the number with the largest distance being mapped to 1 if its a positive number or -1 if its a negative number.The distance from 0 will affect the distribution with a majority of numbers that are closer together normalizing towards 0.", GH_ParamAccess.item, false);

            NormalizeMode_Param NModeParam = new NormalizeMode_Param();
            NModeParam.NormalizeMode = NormalizeMode.MinMax;
            NormalizeModeInt = (int)NModeParam.NormalizeMode;
            NModeParam.SetPersistentData(NormalizeModeInt);
            pManager.AddParameter(NModeParam, "Normalization Mode", "Mode", "Normalization Mode: \n (0) Min Max - rescale the input by the difference between the minimum and maximum values in the data.\n (1) Mean Variance - subtract the mean (of the data) and divide by the variance (of the data)\n (2) Log Mean Variance - normalize based on the logarithm of the data\n (3) Normalize Binning - assign the input value to a bin index and divide by the number of bins to produce a float value between 0 and 1. The bin boundaries are calculated to evenly distribute the data across bins.", GH_ParamAccess.item);
                    
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }

        protected override void BeforeSolveInstance()
        {
            Refresh = true;
        }

        private void SolutionCallback(GH_Document doc)
        {
            UpdateParams();
            ExpireSolution(false);
        }

        private void UpdateParams()
        {
            GH_ComponentParamServer.IGH_SyncObject syncObj = Params.EmitSyncObject();

            //Remove superflous inputs
            while (Params.Input.Count > 4)
            {
                Params.UnregisterInputParameter(Params.Input[Params.Input.Count - 1], true);
            }

            if (p_inputs.Length > 0)
            {
                for (int i = 0; i < p_inputs.Length; i += 5)
                {

                    IGH_Param param;
                    if (p_inputs[i] == "int") param = new Param_Integer();
                    else if (p_inputs[i] == "double") param = new Param_Number();
                    else if (p_inputs[i] == "boolean") param = new Param_Boolean();
                    else param = new Param_GenericObject();

                    param.MutableNickName = false;
                    param.Name = p_inputs[i + 1];
                    param.NickName = p_inputs[i + 2];
                    param.Description = p_inputs[i + 3];
                    param.Access = GH_ParamAccess.item;
                    param.Optional = true;
                    if (p_inputs[i] == "boolean")
                    {
                        var p = param as Param_Boolean;
                        p.AddVolatileData(new GH_Path(0), 0, Convert.ToBoolean(p_inputs[i + 4]));
                    }
                    else if (p_inputs[i] == "double")
                    {
                        var p = param as Param_Number;
                        p.AddVolatileData(new GH_Path(0), 0, Convert.ToDouble(p_inputs[i + 4]));
                    }
                    else if (p_inputs[i] == "int")
                    {
                        var p = param as Param_Integer;
                        p.AddVolatileData(new GH_Path(0), 0, Convert.ToInt32(p_inputs[i + 4]));
                    }
                    else
                    {
                        param.AddVolatileData(new GH_Path(0), 0, Convert.ToString(p_inputs[i + 4]));
                    }

                    Params.RegisterInputParam(param);
                }
            }
            Params.Sync(syncObj);
            this.OnAttributesChanged();
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetBoolean("m_useCDF", useCDF);
            writer.SetInt32("m_maxBins", maxBins);
            return base.Write(writer);
        }

        public override bool Read(GH_IReader reader)
        {
            int m_bins = 16;
            bool m_useCDF = true;
            if (reader.TryGetInt32("m_maxBins", ref m_bins))
            {
                maxBins = m_bins;
            }
            if (reader.TryGetBoolean("m_useCDF", ref m_useCDF))
            {
                useCDF = m_useCDF;
            }

            return base.Read(reader);
        }
        #endregion

        public int NormalizeModeInt = 1;
        public int p_NormalizeModeInt = -1;
        public bool ShowNormalizeModeMenu = true;
        public bool NeedsUpdate = false;
        public bool Refresh;
        private string[] p_inputs = new string[] { };
        private bool fixZero = true;
        private bool useCDF = true;
        private int maxBins = 16;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if (!Refresh)
            {
                DA.DisableGapLogic();
                return;
            }
            Refresh = false;

            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> columnName = new List<string>();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, columnName) &&
                    DA.GetData<bool>(2, ref fixZero)
                )
            {
                NormalizeMode_Param NModeParam = this.Params.Input[3] as NormalizeMode_Param;
                NormalizeMode nMode = NModeParam.NormalizeMode;
                NormalizeModeInt = (int)nMode;

                if (NModeParam.SourceCount > 0)
                {
                    DA.GetData<int>(3, ref NormalizeModeInt);
                    if (NormalizeModeInt >= (int)NormalizeMode.Count) NormalizeModeInt = (int)NormalizeMode.Count - 1;
                    else if (NormalizeModeInt <= 0) NormalizeModeInt = 0;

                    nMode = (NormalizeMode)NormalizeModeInt;
                    ShowNormalizeModeMenu = false;
                }
                else
                {
                    NModeParam.PersistentData[0][0].Value = NormalizeModeInt;
                    ShowNormalizeModeMenu = true;
                }


                if (nMode == NormalizeMode.MeanVariance || nMode == NormalizeMode.LogMeanVariance)
                {
                    p_inputs = new string[5] {
                    "boolean", "Use CDF", "Use CDF", "Whether to use a Cumulative Distribution Function (CDF) as the output.", "true"};
                }
                else if (nMode == NormalizeMode.NormalizeBinning)
                {
                    p_inputs = new string[5] {
                    "int", "Max Bin Count", "Max Bin Count", "Maximum number of bins. Note: a power of 2 is recommended.", "16"};
                }
                else
                {
                    p_inputs = new string[] { };
                }

                if (NormalizeModeInt != p_NormalizeModeInt)
                {
                    NeedsUpdate = true;
                    p_NormalizeModeInt = NormalizeModeInt;
                }
                else
                {
                    NeedsUpdate = false;
                }

                if (NeedsUpdate)
                {
                    OnPingDocument().ScheduleSolution(1, SolutionCallback);
                    return;
                }

                //set param values if needed
                if (nMode == NormalizeMode.MeanVariance || nMode == NormalizeMode.LogMeanVariance)
                {
                    var param4 = this.Params.Input[4] as Param_Boolean;
                    if (param4.SourceCount == 0 && param4.PersistentDataCount == 0)
                    {
                        param4.AddVolatileData(new GH_Path(0), 0, useCDF);
                    }
                }
                else if (nMode == NormalizeMode.NormalizeBinning)
                {
                    var param4 = this.Params.Input[4] as Param_Integer;
                    if (param4.SourceCount == 0 && param4.PersistentDataCount == 0)
                    {
                        param4.AddVolatileData(new GH_Path(0), 0, maxBins);
                    }
                }

                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;
                
                InputOutputColumnPair[] inputOutputNormalized = new InputOutputColumnPair[columnName.Count];

                for (int i = 0; i < columnName.Count; i++)
                {
                    inputOutputNormalized[i] = new InputOutputColumnPair(columnName[i] + "_normalized", columnName[i]);
                }

                if (nMode == NormalizeMode.MinMax)
                {
                    var estimator = mlContext.Transforms.NormalizeMinMax(inputOutputNormalized, fixZero: fixZero);
                    var transformer = estimator.Fit(data);
                    var transformedData = transformer.Transform(data);
                    DA.SetData(0, transformedData);
                }
                else if (nMode == NormalizeMode.MeanVariance)
                {
                    DA.GetData<bool>(4, ref useCDF);
                    var estimator = mlContext.Transforms.NormalizeMeanVariance(inputOutputNormalized, fixZero: fixZero, useCdf: useCDF);
                    var transformer = estimator.Fit(data);
                    var transformedData = transformer.Transform(data);
                    DA.SetData(0, transformedData);
                }
                else if (nMode == NormalizeMode.LogMeanVariance)
                {
                    DA.GetData<bool>(4, ref useCDF);
                    var estimator = mlContext.Transforms.NormalizeMeanVariance(inputOutputNormalized, fixZero: fixZero, useCdf: useCDF);
                    var transformer = estimator.Fit(data);
                    var transformedData = transformer.Transform(data);
                    DA.SetData(0, transformedData);
                }
                else if (nMode == NormalizeMode.NormalizeBinning)
                {
                    DA.GetData<int>(4, ref maxBins);
                    var estimator = mlContext.Transforms.NormalizeBinning(inputOutputNormalized, fixZero: fixZero, maximumBinCount: maxBins);
                    var transformer = estimator.Fit(data);
                    var transformedData = transformer.Transform(data);
                    DA.SetData(0, transformedData);
                }
               
                p_NormalizeModeInt = NormalizeModeInt;
            }
        }
        #endregion
    }
}



