﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using ProvingGround.MachineLearning.Parsers;
using System.Drawing.Drawing2D;
using ProvingGround.MachineLearning.Classes.DataStructures;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Image Detection
    /// </summary>
    public class MLnodeONNXImageClassifier : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeONNXImageClassifier()
            : base("Object Detection Using ONNX File", "Image Classifier", "Use a pre-trained deep learning Open Neural Network Exchange (ONNX) file for object detection and image segmentation. Object detection models detect the presence of multiple objects in an image and segment out areas of the image where the objects are detected. Semantic segmentation models partition an input image by labeling each pixel into a set of pre-defined categories. For more information visit http://https://onnx.ai/", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("b3f24117-156c-42fa-b24e-e9bc2da27906"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_ImageSegmentation; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Bitmap Path", "Bmp", "The full path to a image file.", GH_ParamAccess.item);
            pManager.AddTextParameter("Model Path", "Model", "The file path to the pre-trained ONNX Model. This component works with the YOLO (You Only Look Once) V4 model which can be downloaded at https://github.com/onnx/models/tree/master/vision/object_detection_segmentation/yolov4. The YOLO V4 model is a pre-trained deep convolutional neural network for real-time object detection that detects 80 different classes.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Bitmap with Regions", "Bmp", "The bitmap with bounding boxes drawn on the image.", GH_ParamAccess.item);
            pManager.AddRectangleParameter("Image Boundary", "Boundary", "A rectangle representing the boundary of the image.The boundary has been normalized such that the maximum dimension of the image (ie. width or height) fall within the [0-1] domain.", GH_ParamAccess.item);
            pManager.AddRectangleParameter("Image Regions", "Regions", "Rectangles regions representing classified areas of interest in the image. The rectangles have been normalized such that the maximum dimension of the image (ie. width or height) fall within the [0-1] domain.", GH_ParamAccess.list);
            pManager.AddGenericParameter("Statistics", "Stats", "Statistics for the image classification.", GH_ParamAccess.list);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string imageFilePath = "";
            string modelFilePath = "";
            List<RectangleF> rectangles = new List<RectangleF>();

            if (
                    DA.GetData<string>(0, ref imageFilePath) &&
                    DA.GetData<string>(1, ref modelFilePath)
               )
            {
                var mlContext = PG_MLContext.Instance;
                List<string> log = new List<string>();


                try
                {
                    // Create instance of model scorer
                    var modelScorer = new Yolo4ModelScorer(imageFilePath, modelFilePath, mlContext);

                    // Use model to score data
                    IReadOnlyList<YoloV4Result> results = modelScorer.Score();
                    log.Add(LogYolo4DetectedObjects(imageFilePath, results));
                    var image = DrawYolo4BoundingBox(imageFilePath, results);
                    RectangleF boundary = new RectangleF();
                    rectangles = GetYolo4BoundingBox(imageFilePath, results, ref boundary);

                    DA.SetData(0, image);
                    DA.SetData(1, boundary);
                    DA.SetDataList(2, rectangles);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


                // use this for TinyYolo2 models
                /*try
                {
                    // Load Data
                    IEnumerable<ImageNetData> images = ImageNetData.ReadFile(imageFilePath);

                    IDataView imageDataView = mlContext.Data.LoadFromEnumerable(images);

                    // Create instance of model scorer
                    var modelScorer = new TinyYolo2ModelScorer(imageFilePath, modelFilePath, mlContext);

                    // Use model to score data
                    IEnumerable<float[]> probabilities = modelScorer.Score(imageDataView);

                    // Post-process model output
                    TinyYolo2OutputParser parser = new TinyYolo2OutputParser();

                    var boundingBoxes =
                        probabilities
                        .Select(probability => parser.ParseOutputs(probability))
                        .Select(boxes => parser.FilterBoundingBoxes(boxes, 5, .5F));


                    IList<TinyYolo2BoundingBox> detectedObjects = boundingBoxes.ElementAt(0);
                    var image = DrawTinyYolo2BoundingBox(imageFilePath, detectedObjects);
                    DA.SetData(1, image);
                    log.Add(LogTinyYolo2DetectedObjects(imageFilePath, detectedObjects));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }*/
                DA.SetDataList(3, log);
            }
        }

        private static Image DrawTinyYolo2BoundingBox(string inputImageLocation, IList<TinyYolo2BoundingBox> filteredBoundingBoxes)
        {
            Image image = Image.FromFile(inputImageLocation);

            var originalImageHeight = image.Height;
            var originalImageWidth = image.Width;

            foreach (var box in filteredBoundingBoxes)
            {
                // Get Bounding Box Dimensions
                var x = (uint)Math.Max(box.Dimensions.X, 0);
                var y = (uint)Math.Max(box.Dimensions.Y, 0);
                var width = (uint)Math.Min(originalImageWidth - x, box.Dimensions.Width);
                var height = (uint)Math.Min(originalImageHeight - y, box.Dimensions.Height);

                // Resize To Image
                x = (uint)originalImageWidth * x / TinyYolo2ModelScorer.ImageNetSettings.imageWidth;
                y = (uint)originalImageHeight * y / TinyYolo2ModelScorer.ImageNetSettings.imageHeight;
                width = (uint)originalImageWidth * width / TinyYolo2ModelScorer.ImageNetSettings.imageWidth;
                height = (uint)originalImageHeight * height / TinyYolo2ModelScorer.ImageNetSettings.imageHeight;

                // Bounding Box Text
                string text = $"{box.Label} ({(box.Confidence * 100).ToString("0")}%)";

                using (Graphics thumbnailGraphic = Graphics.FromImage(image))
                {
                    thumbnailGraphic.CompositingQuality = CompositingQuality.HighQuality;
                    thumbnailGraphic.SmoothingMode = SmoothingMode.HighQuality;
                    thumbnailGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    // Define Text Options
                    Font drawFont = new Font("Arial", 12, FontStyle.Bold);
                    SizeF size = thumbnailGraphic.MeasureString(text, drawFont);
                    SolidBrush fontBrush = new SolidBrush(Color.Black);
                    Point atPoint = new Point((int)x, (int)y - (int)size.Height - 1);

                    // Define BoundingBox options
                    Pen pen = new Pen(box.BoxColor, 3.2f);
                    SolidBrush colorBrush = new SolidBrush(box.BoxColor);

                    // Draw text on image 
                    thumbnailGraphic.FillRectangle(colorBrush, (int)x, (int)(y - size.Height - 1), (int)size.Width, (int)size.Height);
                    thumbnailGraphic.DrawString(text, drawFont, fontBrush, atPoint);

                    // Draw bounding box on image
                    thumbnailGraphic.DrawRectangle(pen, x, y, width, height);
                }
            }

            return image;
        }

        private static Image DrawYolo4BoundingBox(string inputImageLocation, IReadOnlyList<YoloV4Result> results)
        {
            Image image = Image.FromFile(inputImageLocation);

            var originalImageHeight = image.Height;
            var originalImageWidth = image.Width;

            foreach (var res in results)
            {
                // Get Bounding Box Dimensions
                var x1 = res.BBox[0];
                var y1 = res.BBox[1];
                var x2 = res.BBox[2];
                var y2 = res.BBox[3];

                // Bounding Box Text
                string text = $"{res.Label} ({(res.Confidence * 100).ToString("0")}%)";

                using (Graphics thumbnailGraphic = Graphics.FromImage(image))
                {
                    thumbnailGraphic.CompositingQuality = CompositingQuality.HighQuality;
                    thumbnailGraphic.SmoothingMode = SmoothingMode.HighQuality;
                    thumbnailGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    // Define Text Options
                    Font drawFont = new Font("Arial", 9, FontStyle.Bold);
                    SizeF size = thumbnailGraphic.MeasureString(text, drawFont);
                    SolidBrush fontBrush = new SolidBrush(Color.Black);
                    Point atPoint = new Point((int)x1, (int)y1 - (int)size.Height - 1);

                    // Define BoundingBox options
                    Pen pen = new Pen(res.BoxColor, 3.2f);
                    SolidBrush colorBrush = new SolidBrush(res.BoxColor);

                    // Draw text on image 
                    thumbnailGraphic.FillRectangle(colorBrush, (int)x1, (int)(y1 - size.Height - 1), (int)size.Width, (int)size.Height);
                    thumbnailGraphic.DrawString(text, drawFont, fontBrush, atPoint);

                    // Draw bounding box on image
                    thumbnailGraphic.DrawRectangle(pen, x1, y1, x2 - x1, y2 - y1);
                    using (var brushes = new SolidBrush(Color.FromArgb(50, res.BoxColor)))
                    {
                        thumbnailGraphic.FillRectangle(brushes, x1, y1, x2 - x1, y2 - y1);
                    }
                }
            }

            return image;
        }

        private static List<RectangleF> GetYolo4BoundingBox(string inputImageLocation, IReadOnlyList<YoloV4Result> results, ref RectangleF boundary)
        {
            Image image = Image.FromFile(inputImageLocation);
            List<RectangleF> rectangles = new List<RectangleF>();

            var originalImageHeight = image.Height;
            var originalImageWidth = image.Width;
            var maxDim = Math.Max(originalImageWidth, originalImageHeight);
            var dt = 1.0 / (double)maxDim;

            //Add bounding box for image frame
            boundary = new RectangleF(0.0f, 0.0f, (float)(originalImageWidth * dt), (float)(originalImageHeight * dt));

            foreach (var res in results)
            {
                // Get Bounding Box Dimensions
                var x1 = res.BBox[0] * dt;
                var y1 = res.BBox[1] * dt;
                var x2 = res.BBox[2] * dt;
                var y2 = res.BBox[3] * dt;

                rectangles.Add(new RectangleF((float)x1, (float)((originalImageHeight * dt) - y1), (float)(x2 - x1), -(float)(y2 - y1)));
            }

            return rectangles;
        }

        private static string LogTinyYolo2DetectedObjects(string imageName, IList<TinyYolo2BoundingBox> boundingBoxes)
        {
            string output = "";
            output += $".....The objects in the image {imageName} are detected as below....\n";

            foreach (var box in boundingBoxes)
            {
                output += $"{box.Label} and its Confidence score: {box.Confidence}\n";
            }

            output += "\n";
            return output;
        }

        private static string LogYolo4DetectedObjects(string imageName, IReadOnlyList<YoloV4Result> boundingBoxes)
        {
            string output = "";
            output += $".....The objects in the image {imageName} are detected as below....\n";

            foreach (var box in boundingBoxes)
            {
                output += $"{box.Label} and its Confidence score: {box.Confidence}\n";
            }

            output += "\n";
            return output;
        }
        #endregion
    }
}



