﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Windows.Forms;
using GH_IO.Serialization;
using Microsoft.ML.Transforms;

namespace ProvingGround.MachineLearning
{
    public enum OHEOutputKind
    {
        Indicator = 2,
        Bag = 1,
        Key = 3,
        Binary = 4,
        Count = 5
    }

    public class EncodingOutput_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("b6690c92-8e4e-4815-86ac-de45805f4365"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            MLnodeOneHotEncoding parent = this.Attributes.Parent.DocObject as MLnodeOneHotEncoding;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Output Kind";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 1; i < (int)OHEOutputKind.Count; i++)
            {
                Menu_AppendItem(menu, ((OHEOutputKind)i).ToString(), MenuItem_Clicked, parent.ShowMenu, parent.ShowMenu && (OutputKind == ((OHEOutputKind)i))).Tag = (OHEOutputKind)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                OutputKind = (OHEOutputKind)ts.Tag;
                MLnodeOneHotEncoding parent = this.Attributes.Parent.DocObject as MLnodeOneHotEncoding;
                parent.EncodingInt = (int)OutputKind;
                this.PersistentData[0][0].Value = (int)OutputKind;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_outputKind", (int)OutputKind);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int nmode = 0;
            if (reader.TryGetInt32("m_outputKind", ref nmode))
            {
                OutputKind = (OHEOutputKind)nmode;
            }
            return base.Read(reader);
        }

        public OHEOutputKind OutputKind
        {
            get { return m_outputKind; }
            set { m_outputKind = value; }
        }

        private OHEOutputKind m_outputKind = OHEOutputKind.Indicator;
    }


    /// <summary>
    /// One Hot Encoding Component
    /// </summary>
    public class MLnodeOneHotEncoding : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeOneHotEncoding()
            : base("One Hot Encoding", "Encoding", "Converts one or more input text columns into as many columns of one-hot encoded vectors. One-hot encoding is often used to convert categorical data into a form that can be provided to a machine learning algorithm.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("7bb44991-067e-4e90-aab0-8eb1a72a5a7c"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_OneHotEncoding; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to change.", GH_ParamAccess.item);
            pManager.AddTextParameter("Columns", "Columns", "The names of the columns in the data set to encode.", GH_ParamAccess.list);

            EncodingOutput_Param encodingParam = new EncodingOutput_Param();
            encodingParam.OutputKind = OHEOutputKind.Indicator;
            EncodingInt = (int)encodingParam.OutputKind;
            encodingParam.SetPersistentData(EncodingInt);
            pManager.AddParameter(encodingParam, "Output Kind", "Output Kind", "Output Kind: \n" +
                " (1) Bag - produces one vector such that each slot stores the number of occurances of the corresponding value in the input vector. Each slot in this vector corresponds to a value in the dictionary, so its length is the size of the built dictionary. Indicator and Bag differ simply in how the bit-vectors generated from individual slots in the input column are aggregated: for Indicator they are concatenated and for Bag they are added. When the source column is a Scalar, the Indicator and Bag options are identical.\n" +
                " (2) Indicator - produces an indicator vector. Each slot in this vector corresponds to a category in the dictionary, so its length is the size of the built dictionary. If a value is not found in the dictioray, the output is the zero vector.\n" +
                " (3) Key - produces keys in a KeyDataViewType column. If the input column is a vector, the output contains a vectory key type, where each slot of the vector corresponds to the respective slot of the input vector. If a category is not found in the bulit dictionary, it is assigned the value zero.\n" +
                " (4) Binary - produces a binary encoded vector to represent the values found in the dictionary that are present in the input column. If a value in the input column is not found in the dictionary, the output is the zero vector.\n", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }
        #endregion

        public int EncodingInt = 0;
        public bool ShowMenu = true;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> columnName = new List<string>();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, columnName)
                )
            {
                EncodingOutput_Param encodingParam = this.Params.Input[2] as EncodingOutput_Param;
                OHEOutputKind outputKind = encodingParam.OutputKind;
                EncodingInt = (int)outputKind;

                if (encodingParam.SourceCount > 0)
                {
                    DA.GetData<int>(2, ref EncodingInt);
                    if (EncodingInt >= (int)OHEOutputKind.Count) EncodingInt = (int)OHEOutputKind.Count - 1;
                    else if (EncodingInt <= 1) EncodingInt = 1;

                    outputKind = (OHEOutputKind)EncodingInt;
                    ShowMenu = false;
                }
                else
                {
                    encodingParam.PersistentData[0][0].Value = EncodingInt;
                    ShowMenu = true;
                }

                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;

                InputOutputColumnPair[] inputOutput = new InputOutputColumnPair[columnName.Count];

                for (int i = 0; i < columnName.Count; i++)
                {
                    inputOutput[i] = new InputOutputColumnPair(columnName[i] + "_encoded", columnName[i]);
                }
                var output = (OneHotEncodingEstimator.OutputKind)EncodingInt;

                var pipeline = mlContext.Transforms.Categorical.OneHotEncoding(inputOutput, outputKind: (OneHotEncodingEstimator.OutputKind)EncodingInt);
                var transformer = pipeline.Fit(data);
                IDataView transformedData = transformer.Transform(data);

                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



