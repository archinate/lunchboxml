﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodePeekAtData : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodePeekAtData()
            : base("Peek At Data", "Peek Data", "Peek at a small sample of the contents of a ML data set", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("7c3a6b0a-9c51-4739-b087-4622f7947377"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_PeekAtData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The loaded data set.", GH_ParamAccess.item);
            pManager.AddIntegerParameter("Number of Rows", "Rows", "Number of rows in the data set to view. Note: this component can only return a maximum of 100 rows.", GH_ParamAccess.item, 5);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddTextParameter("Values", "Values", "The data values.", GH_ParamAccess.tree);
            pManager.AddTextParameter("Columns", "Columns", "The names of each column in the data set.", GH_ParamAccess.list);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            int numLines = 0;
            List<string> columnNames = new List<string>();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<int>(1, ref numLines)
               )
            {
                if (numLines < 0) numLines = 0;
                else if (numLines > 100) numLines = 100;

                DataTree<string> values = new DataTree<string>();

                IDataView data = _data.Value as IDataView;
                var selectedRows = data.Preview(numLines);

                int cnt = 0;
                foreach (var row in selectedRows.RowView)
                {
                    var ColumnCollection = row.Values;     
                    GH_Path path = new GH_Path(cnt);
                    foreach (KeyValuePair<string, object> column in ColumnCollection)
                    {
                        values.Add(column.Key + ":" + column.Value.ToString(), path);
                        if (cnt == 0) columnNames.Add(column.Key);
                    }
                    cnt++;
                }

                DA.SetDataTree(0, values);
                DA.SetDataList(1, columnNames);
            }
        }
        #endregion
    }
}



