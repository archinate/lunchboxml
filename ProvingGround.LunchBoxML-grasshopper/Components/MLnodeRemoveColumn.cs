﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeRemoveColumn : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeRemoveColumn()
            : base("Remove Columns", "Remove Columns", "Remove columns from a data set.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("0cd9d423-5ee6-420b-a4f5-c6b3d4b9202f"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_RemoveColumns; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to change.", GH_ParamAccess.item);
            pManager.AddTextParameter("Columns", "Columns", "The names of the columns in the data set to remove.", GH_ParamAccess.list);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> columnName = new List<string>();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, columnName) 
                )
            {
                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;
                var pipeline = mlContext.Transforms.DropColumns(columnName.ToArray());
                IDataView transformedData = pipeline.Fit(data).Transform(data);

                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



