﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Windows.Forms;
using GH_IO.Serialization;
using Microsoft.ML.Transforms;

namespace ProvingGround.MachineLearning
{
    public enum ReplacementMode
    {
        Mean,
        Minimum,
        Maximum,
        Mode,
        Count
    }

    public class ReplacementMode_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("29fa3b21-fe33-43aa-95ec-8bda10536e46"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            MLnodeReplaceMissingData parent = this.Attributes.Parent.DocObject as MLnodeReplaceMissingData;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Replacement Mode";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowReplacementModeMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)ReplacementMode.Count; i++)
            {
                Menu_AppendItem(menu, ((ReplacementMode)i).ToString(), MenuItem_Clicked, parent.ShowReplacementModeMenu, parent.ShowReplacementModeMenu && (ReplacementMode == ((ReplacementMode)i))).Tag = (ReplacementMode)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                ReplacementMode = (ReplacementMode)ts.Tag;
                MLnodeReplaceMissingData parent = this.Attributes.Parent.DocObject as MLnodeReplaceMissingData;
                parent.ReplacementModeInt = (int)ReplacementMode;
                this.PersistentData[0][0].Value = (int)ReplacementMode;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_replacementMode", (int)ReplacementMode);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int rmode = 0;
            if (reader.TryGetInt32("m_replacementMode", ref rmode))
            {
                ReplacementMode = (ReplacementMode)rmode;
            }
            return base.Read(reader);
        }

        public ReplacementMode ReplacementMode
        {
            get { return m_replacementMode; }
            set { m_replacementMode = value; }
        }

        private ReplacementMode m_replacementMode = ReplacementMode.Mean;

    }

    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeReplaceMissingData : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeReplaceMissingData()
            : base("Replace Missing Data", "Replace Data", "Replace missing data in a column. Note: This component only works on numeric data.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("4505b82a-5921-4400-8493-30750d8cd85d"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_ReplaceMissingData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Incoming Data", "Data", "The data set to modify.", GH_ParamAccess.item);
            pManager.AddTextParameter("Column", "Column", "The name of the column in the data set to replace missing items.", GH_ParamAccess.item);
            
            ReplacementMode_Param RModeParam = new ReplacementMode_Param();
            RModeParam.ReplacementMode = ReplacementMode.Mean;
            ReplacementModeInt = (int)RModeParam.ReplacementMode;
            RModeParam.SetPersistentData(ReplacementModeInt);
            pManager.AddParameter(RModeParam, "Replacement Mode", "Replacement Mode", "Replacement Mode: \n (0) Mean - replace with the average of the column\n (1) Minimum - replace with the minimum value of the column\n (2) Maximum - replace with the maximum value of the column\n (3) Mode - replace with the most frequent value of the column", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The filtered data set.", GH_ParamAccess.item);
        }
        #endregion

        public int ReplacementModeInt = 0;
        public bool ShowReplacementModeMenu = true;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            string columnName = "";

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<string>(1, ref columnName) 
               )
            {

                ReplacementMode_Param RModeParam = this.Params.Input[2] as ReplacementMode_Param;
                ReplacementMode rMode = RModeParam.ReplacementMode;
                ReplacementModeInt = (int)rMode;

                if (RModeParam.SourceCount > 0)
                {
                    DA.GetData<int>(2, ref ReplacementModeInt);
                    if (ReplacementModeInt >= (int)ReplacementMode.Count) ReplacementModeInt = (int)ReplacementMode.Count - 1;
                    else if (ReplacementModeInt <= 0) ReplacementModeInt = 0;

                    rMode = (ReplacementMode)ReplacementModeInt;
                    ShowReplacementModeMenu = false;
                }
                else
                {
                    RModeParam.PersistentData[0][0].Value = ReplacementModeInt;
                    ShowReplacementModeMenu = true;
                }

                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;

                MissingValueReplacingEstimator.ReplacementMode estimator = new MissingValueReplacingEstimator.ReplacementMode();
                switch (RModeParam.ReplacementMode)
                {
                    case ReplacementMode.Mean:
                        estimator = MissingValueReplacingEstimator.ReplacementMode.Mean;
                        break;
                    case ReplacementMode.Minimum:
                        estimator = MissingValueReplacingEstimator.ReplacementMode.Minimum;
                        break;
                    case ReplacementMode.Maximum:
                        estimator = MissingValueReplacingEstimator.ReplacementMode.Maximum;
                        break;
                    case ReplacementMode.Mode:
                        estimator = MissingValueReplacingEstimator.ReplacementMode.Mode;
                        break;
                }

                
                var pipeline = mlContext.Transforms.ReplaceMissingValues(new[] { new InputOutputColumnPair(columnName + "_replaced", columnName)}, estimator, true);
                var transformer = pipeline.Fit(data);
                var trasnformedData = transformer.Transform(data);
                DA.SetData(0, trasnformedData);

            }
        }
        #endregion
    }
}



