﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using static Microsoft.ML.DataOperationsCatalog;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeSplitData : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeSplitData()
            : base("Split Data", "Split Data", "Split a loaded data set into training and testing subsets of data.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("e9ce551b-c684-418e-8724-27c1c2e29432"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SplitData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The loaded data set to split", GH_ParamAccess.item);
            pManager.AddNumberParameter("Testing Fraction", "Fraction", "The fraction of original data set to go into the testing set.", GH_ParamAccess.item, 0.1);
            pManager.AddIntegerParameter("Seed", "Seed", "Seed for the random number generator used to select rows for the train-test split.", GH_ParamAccess.item, 1);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Data Set", "Training Data", "The training data set", GH_ParamAccess.item);
            pManager.AddGenericParameter("Testing Data Set", "Testing Data", "The testing data set", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            double fraction = 0.1;
            int seed = 1;
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<double>(1, ref fraction) &&
                    DA.GetData<int>(2, ref seed)
               )
            {
                var mlContext = PG_MLContext.Instance;

                if (fraction < 0.0) fraction = 0.0;
                else if (fraction >= 1.0) fraction = 1.0;

                IDataView data = _data.Value as IDataView;
                TrainTestData splitDataView = mlContext.Data.TrainTestSplit(data, testFraction: fraction, seed: seed);

                DA.SetData(0, splitDataView.TrainSet);
                DA.SetData(1, splitDataView.TestSet);
            }
        }
        #endregion
    }
}



