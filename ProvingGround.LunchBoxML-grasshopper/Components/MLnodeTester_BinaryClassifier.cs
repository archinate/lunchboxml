﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using Grasshopper;

namespace ProvingGround.MachineLearning
{
    public class ModelOutput
    {
        public bool PredictedLabel { get; set; }
        public float Score { get; set; }
    }

    /// <summary>
    /// Binary Classifier Tester Component
    /// </summary>
    public class MLnodeTesterBinaryClassifier : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTesterBinaryClassifier()
            : base("Binary Classifier Tester", "Binary Classifier Tester", "Test a model for binary classification problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("c915a6ef-582c-48cc-909a-ea231cf765c9"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_BinaryClassifierTester; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Model", "Model", "Trained binary classification model.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Testing Data Set", "Testing Data", "The testing data set.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Predictions", "Predictions", "Predictions", GH_ParamAccess.tree);
            pManager.Register_DoubleParam("Scores", "Scores", "The scores of all classes. Higher value means higher probability to fall into the associated class. If the i-th element has the largest value, the predicted label index would be i.", GH_ParamAccess.tree);
            //pManager.Register_GenericParam("Confusion Matrix", "Confusion Matrix", "The confusion matrix giving the counts of the true positives, true negatives, false positives and false negatives for the two classes of data.");
            //pManager.Register_DoubleParam("Accuracy", "Accuracy", "Gets the accuracy of a classifier which is the proportion of correct predictions in the test set. The closer to 1.00, the better.");         
            //pManager.Register_DoubleParam("F1 Score", "F1 Score", "Gets the F1 score of the classifier, which is a measure of the classifier's quality considering both precision and recall. The closer to 1.00, the better.");      
            //pManager.Register_DoubleParam("Negative Precision", "Neg Precision", "Gets the negative precision of a classifier which is the proportion of correctly predicted negative instances among all the negative predictions (i.e., the number of negative instances predicted as negative, divided by the total number of instances predicted as negative).");
            //pManager.Register_DoubleParam("Negative Recall", "Neg Recall", "Gets the negative recall of a classifier which is the proportion of correctly predicted negative instances among all the negative instances (i.e., the number of negative instances predicted as negative, divided by the total number of negative instances).");
            //pManager.Register_DoubleParam("Positive Precision", "Pos Precision", "Gets the positive precision of a classifier which is the proportion of correctly predicted positive instances among all the positive predictions (i.e., the number of positive instances predicted as positive, divided by the total number of instances predicted as positive).");
            //pManager.Register_DoubleParam("Positive Recall", "Pos Recall", "Gets the positive recall of a classifier which is the proportion of correctly predicted positive instances among all the positive instances (i.e., the number of positive instances predicted as positive, divided by the total number of positive instances).");
            //pManager.Register_DoubleParam("Entropy", "Entropy", "Gets the test-set entropy, which is the prior log-loss based on the proportion of positive and negative instances in the test set. A classifier's LogLoss lower than the entropy indicates that a classifier does better than predicting the proportion of positive instances as the probability for each instance.");
            //pManager.Register_DoubleParam("Log Loss", "Log Loss", "Gets the log-loss of the classifier. Log-loss measures the performance of a classifier with respect to how much the predicted probabilities diverge from the true class label. Lower log-loss indicates a better model. A perfect model, which predicts a probability of 1 for the true class, will have a log-loss of 0.");
            //pManager.Register_DoubleParam("Log Loss Reduction", "Loss Reduction", "Gets the log-loss reduction (also known as relative log-loss, or reduction in information gain - RIG) of the classifier. It gives a measure of how much a model improves on a model that gives random predictions. Log-loss reduction closer to 1 indicates a better model.");

        }
        #endregion


        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            GH_ObjectWrapper _model = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _model) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref _data)
                )
            {
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                ITransformer model = _model.Value as ITransformer;

                var properties = new List<DynamicTypeProperty>();
                foreach (var column in data.Schema)
                {
                    properties.Add(new DynamicTypeProperty(column.Name, column.Type.RawType));
                }

                // create the new type
                var dynamicType = DynamicType.CreateDynamicType(properties);
                var dynamicList = DynamicType.CreateDynamicList(dynamicType);
                var inputSchema = SchemaDefinition.Create(dynamicType);
                var outputSchema = SchemaDefinition.Create(typeof(ModelOutput));

                var modelType = mlContext.Model.GetType();
                var createEngineGeneric = modelType.GetMethods().First(method => method.Name == "CreatePredictionEngine" && method.IsGenericMethod);
                var genericEngineMethod = createEngineGeneric.MakeGenericMethod(dynamicType, typeof(ModelOutput));
                object[] engineArgs = new object[] { model, true, inputSchema, outputSchema };
                dynamic engine = (dynamic)genericEngineMethod.Invoke(mlContext.Model, engineArgs);

                var dataType = mlContext.Data.GetType();
                var createEnumerableGeneric = dataType.GetMethods().First(method => method.Name == "CreateEnumerable" && method.IsGenericMethod);
                var genericEnumerableMethod = createEnumerableGeneric.MakeGenericMethod(dynamicType);
                object[] enumerableArgs = new object[] { data, true, false, inputSchema };
                dynamic enumerableData = (dynamic)genericEnumerableMethod.Invoke(mlContext.Data, enumerableArgs);

                DataTree<bool> predictedLabels = new DataTree<bool>();
                DataTree<double> scores = new DataTree<double>();

                int p = 0;
                foreach (var row in enumerableData)
                {
                    ModelOutput prediction = engine.Predict(row);
                    GH_Path path = new GH_Path(p);
                    predictedLabels.Add((bool)prediction.PredictedLabel, path);
                    scores.Add((double)prediction.Score, path);
                    p++;
                }

                DA.SetDataTree(0, predictedLabels);
                DA.SetDataTree(1, scores);



                //bool isCalibrated = false;



                //IDataView predictions = model.Transform(data);

                /*var outputSchema = predictions.Schema;
                foreach(var column in outputSchema)
                {
                    if (column.Name == "Probability")
                    {
                        isCalibrated = true;
                        break;
                    }
                    else
                    {
                        isCalibrated = false;
                    }
                }

                if (isCalibrated)
                {
                    CalibratedBinaryClassificationMetrics metrics = mlContext.BinaryClassification.Evaluate(predictions, labelColumnName: "Label", scoreColumnName: "Score");
                    DA.SetData(1, metrics.ConfusionMatrix);
                    DA.SetData(2, metrics.Accuracy); 
                    DA.SetData(3, metrics.F1Score);
                    DA.SetData(4, metrics.NegativePrecision);
                    DA.SetData(5, metrics.NegativeRecall);
                    DA.SetData(6, metrics.PositivePrecision);
                    DA.SetData(7, metrics.PositiveRecall);
                    DA.SetData(8, metrics.Entropy);
                    DA.SetData(9, metrics.LogLoss);
                    DA.SetData(10, metrics.LogLossReduction);
                }
                else
                {
                    BinaryClassificationMetrics metrics = mlContext.BinaryClassification.EvaluateNonCalibrated(predictions, labelColumnName: "Label", scoreColumnName: "Score");
                    DA.SetData(1, metrics.ConfusionMatrix);
                    DA.SetData(2, metrics.Accuracy);
                    DA.SetData(3, metrics.F1Score);
                    DA.SetData(4, metrics.NegativePrecision);
                    DA.SetData(5, metrics.NegativeRecall);
                    DA.SetData(6, metrics.PositivePrecision);
                    DA.SetData(7, metrics.PositiveRecall);
                    DA.SetData(8, Double.NaN);
                    DA.SetData(9, Double.NaN);
                    DA.SetData(10, Double.NaN);
                }*/

                /*List<bool> predictedLabels = predictions.GetColumn<bool>("PredictedLabel").ToList();
                DataTree<bool> labels = new DataTree<bool>();
                for (int i = 0; i < predictedLabels.Count; i++)
                {
                    labels.Add(predictedLabels[i], new GH_Path(i));
                }

                DA.SetDataTree(0, labels);*/

            }
        }

        #endregion
    }


}



