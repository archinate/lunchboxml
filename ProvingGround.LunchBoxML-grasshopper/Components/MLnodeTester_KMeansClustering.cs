﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using Grasshopper;
using Microsoft.ML.Trainers;
using Rhino.Geometry;

namespace ProvingGround.MachineLearning
{
    public class ClusterPrediction
    {
        public uint Label { get; set; }
        public uint PredictedLabel { get; set; }
    }

    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeTesterKMeansClustering : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTesterKMeansClustering()
            : base("K-Means Clustering Tester", "K-Means Clustering Tester", "Test a model for K-Means clustering problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("76066889-2211-4dc9-a52b-44e551fef58d"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        /*protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LinearRegression; }
        }*/
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Model", "Model", "Trained Regression model.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Testing Data Set", "Testing Data", "The testing data set.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Predictions", "Predictions", "Predictions", GH_ParamAccess.tree);
            pManager.Register_PointParam("Cluster Centroids", "Centroids", "Returns the centroid of each cluster.", GH_ParamAccess.list);
            pManager.Register_DoubleParam("Average Distance", "Avg Distance", "For the K-Means algorithm, the 'score' is the distance from the centroid to the example. The average score is, therefore, a measure of proximity of the examples to cluster centroids. In other words, it is a measure of 'cluster tightness'. Note however, that this metric will only decrease if the number of clusters is increased, and in the extreme case (where each distinct example is its own cluster) it will be equal to zero.");
            pManager.Register_DoubleParam("Davies-Bouldin Index", "Davies-Bouldin", "The Davies-Bouldin Index is measure of the how much scatter is in the cluster and the cluster separation.");
        }
        #endregion


        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            GH_ObjectWrapper _model = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _model) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref _data)
                )
            {
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                ITransformer model = _model.Value as ITransformer;

                IDataView predictions = model.Transform(data);
                var metrics = mlContext.Clustering.Evaluate(predictions);

                /*List<int> scoreColumn = predictions.GetColumn<int>("PredictedLabel").ToList();
                DataTree<int> scores = new DataTree<int>();
                for (int i = 0; i < scoreColumn.Count; i++)
                {
                    scores.Add(scoreColumn[i], new GH_Path(i));
                }*/

                /*List<Point3d> centroidPoints = new List<Point3d>();
                VBuffer<float>[] centroids = default;
                var last = model.LastTransformer; 
                var modelParams = last.Model as KMeansModelParameters;
                modelParams.GetClusterCentroids(ref centroids, out int k);

                for(int i = 0; i < centroids.Length; i++)
                {
                    float[] vals = centroids[i].GetValues().ToArray();
                    centroidPoints.Add(new Point3d(vals[0], vals[1], vals[2]));
                }*/

                /*DataViewSchema.Builder builder = new DataViewSchema.Builder();
                builder.AddColumn("PredictedLabel", NumberDataViewType.UInt32, null);
                builder.AddColumn("Score", new VectorDataViewType(NumberDataViewType.Single, 3), null);
                var outputSchema = builder.ToSchema();

                var predictor = mlContext.Model.CreatePredictionEngine(model, true, data.Schema, outputSchema);*/

                //DA.SetDataTree(0, scores);
                //DA.SetDataList(1, centroidPoints);
                DA.SetData(2, metrics.AverageDistance);
                DA.SetData(3, metrics.DaviesBouldinIndex);
            }
        }


        #endregion
    }


}



