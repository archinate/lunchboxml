﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.Reflection;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using Grasshopper;
using Rhino.FileIO;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Binary Classifier Tester Component
    /// </summary>
    public class MLnodeTester_MultiClassClassfication : GH_Component
    {
        public class ModelOutput
        {
            public float PredictedLabel { get; set; }
            public float[] Score { get; set; }
        }

        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTester_MultiClassClassfication()
            : base("Multiclass Classifier Tester", "Multiclass Classifier Tester", "Test a model for multiclass classification problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("24619e50-4acd-4bdf-a67b-93598ca7c585"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_MulticlassClassifierTester; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Model", "Model", "Trained binary classification model.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Testing Data Set", "Testing Data", "The testing data set.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Predictions", "Predictions", "Predictions", GH_ParamAccess.tree);
            pManager.Register_DoubleParam("Scores", "Scores", "The scores of all classes. Higher value means higher probability to fall into the associated class. If the i-th element has the largest value, the predicted label index would be i.", GH_ParamAccess.tree);
            
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            GH_ObjectWrapper _model = new GH_ObjectWrapper();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _model) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref _data) 
                )
            {
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                ITransformer model = _model.Value as ITransformer;
                

                // Create runtime type from fields and types in a DataViewSchema
                /*var runtimeType = ClassFactory.CreateType(data.Schema);

                dynamic dynamicPredictionEngine;
                var genericPredictionMethod = mlContext.Model.GetType().GetMethod("CreatePredictionEngine", new[] { typeof(ITransformer), typeof(DataViewSchema) });
                var predictionMethod = genericPredictionMethod.MakeGenericMethod(runtimeType, typeof(ModelOutput));
                dynamicPredictionEngine = predictionMethod.Invoke(mlContext.Model, new object[] { model, data.Schema });
                var result = dynamicPredictionEngine.Predict(data);*/
                
                var properties = new List<DynamicTypeProperty>();
                foreach (var column in data.Schema)
                {
                    properties.Add(new DynamicTypeProperty(column.Name, column.Type.RawType));
                }

                // create the new type
                var dynamicType = DynamicType.CreateDynamicType(properties);
                var dynamicList = DynamicType.CreateDynamicList(dynamicType);
                var inputSchema = SchemaDefinition.Create(dynamicType);
                var outputSchema = SchemaDefinition.Create(typeof(ModelOutput));

                var modelType = mlContext.Model.GetType();
                var createEngineGeneric = modelType.GetMethods().First(method => method.Name == "CreatePredictionEngine" && method.IsGenericMethod);
                var genericEngineMethod = createEngineGeneric.MakeGenericMethod(dynamicType, typeof(ModelOutput));
                object[] engineArgs = new object[]{ model , true, inputSchema, outputSchema };
                dynamic engine = (dynamic)genericEngineMethod.Invoke(mlContext.Model, engineArgs);

                var dataType = mlContext.Data.GetType();
                var createEnumerableGeneric = dataType.GetMethods().First(method => method.Name == "CreateEnumerable" && method.IsGenericMethod);
                var genericEnumerableMethod = createEnumerableGeneric.MakeGenericMethod(dynamicType);
                object[] enumerableArgs = new object[] { data, true, false, inputSchema };
                dynamic enumerableData = (dynamic)genericEnumerableMethod.Invoke(mlContext.Data, enumerableArgs);

                DataTree<double> predictedLabels = new DataTree<double>();
                DataTree<double> scores = new DataTree<double>();
                

                int p = 0;
                foreach (var row in enumerableData)
                {
                    ModelOutput prediction = engine.Predict(row);
                    GH_Path path = new GH_Path(p);
                    predictedLabels.Add((double)prediction.PredictedLabel, path);
                    scores.AddRange(prediction.Score.Select(i => (double)i).ToList(), path);
                    p++;
                }

                DA.SetDataTree(0, predictedLabels);
                DA.SetDataTree(1, scores);
            }
        }

        #endregion
    }


}



