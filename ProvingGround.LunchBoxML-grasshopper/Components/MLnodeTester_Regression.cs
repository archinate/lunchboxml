﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using Grasshopper;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeTesterRegression : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTesterRegression()
            : base("Regression Tester", "Regression Tester", "Test a model for regression problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("cd6d6e47-2059-4dd9-aeac-931954e3be49"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LinearRegressionTester; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Model", "Model", "Trained Regression model.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Testing Data Set", "Testing Data", "The testing data set.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Predictions", "Predictions", "Predictions", GH_ParamAccess.tree);
            pManager.Register_DoubleParam("R2", "R2", "R-squared (R2, also known as the coefficient of determination, represents the predictive power of the model as a value between -inf and 1.00. 1.00 means there is a perfect fit, and the fit can be arbitrarily poor so the scores can be negative. A score of 0.00 means the model is guessing the expected value for the label. R2 measures how close the actual test data values are to the predicted values.");
            pManager.Register_DoubleParam("Absolute Loss", "Abs Loss", "Absolute loss, also known as the Mean absolute error (MAE), measures how close the predictions are to the actual outcomes. It is the average of all the model errors, where model error is the absolute distance between the predicted label value and the correct label value. This prediction error is calculated for each record of the test data set. Finally, the mean value is calculated for all recorded absolute errors.");
            pManager.Register_DoubleParam("Squared Loss", "Sq Loss", "Squared loss, also known as Mean square error (MSE) or Mean squared deviation (MSD), tells you how close a regression line is to a set of test data values by taking the distances from the points to the regression line (these distances are the errors E) and squaring them. The squaring gives more weight to larger differences.");
            pManager.Register_DoubleParam("Root Mean Squared Loss", "RMS Loss", "Root mean squared error (RMSE) measures the difference between values predicted by a model and the values observed from the environment that is being modeled. RMS-loss is the square root of Squared-loss and has the same units as the label, similar to the absolute-loss though giving more weight to larger differences.");
        }
        #endregion


        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            GH_ObjectWrapper _model = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _model) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref _data) 
                )
            {
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                ITransformer model = _model.Value as ITransformer;

                IDataView predictions = model.Transform(data);
                var metrics = mlContext.Regression.Evaluate(predictions, labelColumnName: "Label", scoreColumnName: "Score");

                List<float> scoreColumn = predictions.GetColumn<float>("Score").ToList();
                DataTree<float> scores = new DataTree<float>();
                for(int i = 0; i < scoreColumn.Count; i++)
                {
                    scores.Add(scoreColumn[i], new GH_Path(i));
                }

                DA.SetDataTree(0, scores);
                DA.SetData(1, metrics.RSquared);
                DA.SetData(2, metrics.MeanAbsoluteError);
                DA.SetData(3, metrics.MeanSquaredError);
                DA.SetData(4, metrics.RootMeanSquaredError);
            }
        }


        #endregion
    }


}



