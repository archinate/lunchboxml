﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.IO;
using Grasshopper;
using Grasshopper.Kernel.Data;
using static Microsoft.ML.DataOperationsCatalog;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeTokenizeIntoWords : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTokenizeIntoWords()
            : base("Tokenize Into Words", "Tokenize", "Split one or more text columns into individual words.", "LunchBoxML", "Data Sets")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("a21a767d-26df-401b-a1ca-1bee89a255be"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_TokenizeIntoWords; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The data set to modify.", GH_ParamAccess.item);
            pManager.AddTextParameter("Column", "Column", "The name of the text column in the data set to tokenize.", GH_ParamAccess.item);
            pManager.AddTextParameter("Separators", "Separators", "The character separators to use to split text into words. If empty, the space character will be used.", GH_ParamAccess.list);
            pManager[2].Optional = true;
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Data", "Data", "The modified data set.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            string columnName = "";
            List<string> separators = new List<string>();
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetData<string>(1, ref columnName)
               )
            {
                try
                {
                    DA.GetDataList<string>(2, separators);
                }catch(Exception e) { AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message.ToString()); }

                char[] arr;
                string combined = "";

                if (separators.Count > 0)
                {
                    foreach(string s in separators)
                    {
                        combined += s;
                    }
                    arr = combined.ToCharArray();
                }
                else
                {
                    arr = new[] { ' ' };
                }

                IDataView data = _data.Value as IDataView;
                var mlContext = PG_MLContext.Instance;

                var pipeline = mlContext.Transforms.Text.TokenizeIntoWords(columnName + "_tokenized", columnName, separators:arr);
                var transformer = pipeline.Fit(data);
                IDataView transformedData = transformer.Transform(data);

                DA.SetData(0, transformedData);
            }
        }
        #endregion
    }
}



