﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using System.Windows.Forms;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeTrainerBinaryClassifier : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTrainerBinaryClassifier()
            : base("Binary Classifier Trainer", "Binary Classifier Trainer", "Train a model for binary classification problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("a13c7624-91d4-4eb3-97f8-d54e9be53c83"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_BinaryClassifierTrainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Data Set", "Training Data", "The training data set.", GH_ParamAccess.item);
            pManager.AddTextParameter("Feature Names", "Features", "The names of the columns in the data set to be categorized as features. The inputs to a machine learning model are called features.", GH_ParamAccess.list);
            pManager.AddTextParameter("Label Name", "Label", "The name of the column in the data set to be categorized as the label. The ground-truth values (sometimes known as outputs) are used to train a machine learning model are called labels.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The type of trainer used for regression training.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Model", "Model", "Trained Model");
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Binary Classification Model", SaveModel_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        private void SaveModel_Clicked(object sender, EventArgs e)
        {
            if (trainedModel != null && schema != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Model";
                dialog.Filter = "Zip File (*.zip)|*.zip";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    var mlContext = PG_MLContext.Instance;
                    mlContext.Model.Save(trainedModel, schema, fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid model found. Please create a valid model before trying to save.");
            }
        }
        #endregion

        private ITransformer trainedModel;
        private DataViewSchema schema;
        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> features = new List<string>();
            string label = "";
            GH_ObjectWrapper _trainer = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, features) &&
                    DA.GetData<string>(2, ref label) &&
                    DA.GetData<GH_ObjectWrapper>(3, ref _trainer)
                )
            {

                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                IEstimator<ITransformer> learner = null;

                try
                {
                    if (_trainer.Value is AveragedPerceptronBinaryClassificationTrainer)
                    {
                        AveragedPerceptronBinaryClassificationTrainer trainer = _trainer.Value as AveragedPerceptronBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.AveragedPerceptron(labelColumnName: "Label",
                            featureColumnName: "Features",
                            learningRate: trainer.LearningRate,
                            decreaseLearningRate: trainer.DecreaseLearningRate,
                            numberOfIterations: trainer.NumberOfIterations,
                            l2Regularization: trainer.L2Regularization);
                    }
                    else if (_trainer.Value is FastForestBinaryClassificationTrainer)
                    {
                        FastForestBinaryClassificationTrainer trainer = _trainer.Value as FastForestBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.FastForest(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfTrees: trainer.NumberOfTrees,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf);
                    }
                    else if (_trainer.Value is FastTreeBinaryClassificationTrainer)
                    {
                        FastTreeBinaryClassificationTrainer trainer = _trainer.Value as FastTreeBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.FastTree(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfTrees: trainer.NumberOfTrees,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is FieldAwareFactorizationMachineBinaryClassificationTrainer)
                    {
                        FieldAwareFactorizationMachineBinaryClassificationTrainer trainer = _trainer.Value as FieldAwareFactorizationMachineBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.FieldAwareFactorizationMachine(labelColumnName: "Label",
                            featureColumnName: "Features");
                    }
                    else if (_trainer.Value is GAMBinaryClassificationTrainer)
                    {
                        GAMBinaryClassificationTrainer trainer = _trainer.Value as GAMBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.Gam(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfIterations: trainer.NumberOfIterations,
                            maximumBinCountPerFeature: trainer.MaxBinCountPerFeature,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is LbfgsLogisticRegressionBinaryClassificationTrainer)
                    {
                        LbfgsLogisticRegressionBinaryClassificationTrainer trainer = _trainer.Value as LbfgsLogisticRegressionBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.LbfgsLogisticRegression(labelColumnName: "Label",
                            featureColumnName: "Features",
                            enforceNonNegativity: trainer.EnforceNonNegativity,
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            historySize: trainer.HistorySize,
                            optimizationTolerance: trainer.OptimizationTolerance);
                    }
                    else if (_trainer.Value is LDSVMBinaryClassificationTrainer)
                    {
                        LDSVMBinaryClassificationTrainer trainer = _trainer.Value as LDSVMBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.LdSvm(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfIterations: trainer.NumberOfIterations,
                            treeDepth: trainer.TreeDepth,
                            useBias: trainer.UseBias,
                            useCachedData: trainer.UseCachedData);
                    }
                    else if (_trainer.Value is LightGBMBinaryClassificationTrainer)
                    {
                        LightGBMBinaryClassificationTrainer trainer = _trainer.Value as LightGBMBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.LightGbm(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfIterations: trainer.NumberOfIterations,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is LinearSVMBinaryClassificationTrainer)
                    {
                        LinearSVMBinaryClassificationTrainer trainer = _trainer.Value as LinearSVMBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.LinearSvm(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfIterations: trainer.NumberOfIterations);
                    }
                    else if (_trainer.Value is SDCALogisticRegressionBinaryClassificationTrainer)
                    {
                        SDCALogisticRegressionBinaryClassificationTrainer trainer = _trainer.Value as SDCALogisticRegressionBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "Label",
                            featureColumnName: "Features",
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            maximumNumberOfIterations: trainer.MaxIterations);
                    }
                    else if (_trainer.Value is SDCANoncalibratedBinaryClassificationTrainer)
                    {
                        SDCANoncalibratedBinaryClassificationTrainer trainer = _trainer.Value as SDCANoncalibratedBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.SdcaNonCalibrated(labelColumnName: "Label",
                            featureColumnName: "Features",
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            maximumNumberOfIterations: trainer.MaxIterations);
                    }
                    else if (_trainer.Value is SGDCalibratedBinaryClassificationTrainer)
                    {
                        SGDCalibratedBinaryClassificationTrainer trainer = _trainer.Value as SGDCalibratedBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.SgdCalibrated(labelColumnName: "Label",
                            featureColumnName: "Features",
                            learningRate: trainer.LearningRate,
                            l2Regularization: trainer.L2Regularization,
                            numberOfIterations: trainer.NumberOfIterations);
                    }
                    else if (_trainer.Value is SGDNoncalibratedBinaryClassificationTrainer)
                    {
                        SGDNoncalibratedBinaryClassificationTrainer trainer = _trainer.Value as SGDNoncalibratedBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.SgdNonCalibrated(labelColumnName: "Label",
                            featureColumnName: "Features",
                            learningRate: trainer.LearningRate,
                            l2Regularization: trainer.L2Regularization,
                            numberOfIterations: trainer.NumberOfIterations);
                    }
                    else if (_trainer.Value is SymbolicSGDLogisticRegressionBinaryClassificationTrainer)
                    {
                        SymbolicSGDLogisticRegressionBinaryClassificationTrainer trainer = _trainer.Value as SymbolicSGDLogisticRegressionBinaryClassificationTrainer;
                        learner = mlContext.BinaryClassification.Trainers.SymbolicSgdLogisticRegression(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfIterations: trainer.NumberOfIterations);
                    }
                }
                catch (Exception e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message);
                }

                if (learner != null)
                {
                    // Define data prep estimator
                    var dataProcessPipeline = mlContext.Transforms.CopyColumns(outputColumnName: "Label", inputColumnName: label)
                    .Append(mlContext.Transforms.Concatenate("Features", features.ToArray()));

                    var trainingPipeline = dataProcessPipeline.Append(learner);

                    trainedModel = trainingPipeline.Fit(data);
                    schema = data.Schema;

                    DA.SetData(0, trainedModel);
                }
            }
        }
        #endregion
    }


}



