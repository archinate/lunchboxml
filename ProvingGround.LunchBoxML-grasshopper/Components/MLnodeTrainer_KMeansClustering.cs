﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using System.Windows.Forms;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// K-Means Clustering Trainer
    /// </summary>
    public class MLnodeTrainerKMeansClustering : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTrainerKMeansClustering()
            : base("K-Means Clustering Trainer", "K-Means Clustering Trainer", "Train a model for K-Means clustering problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("d5b1bf2e-3c21-4882-8bca-c3fdceebb57d"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        /*protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LinearRegression; }
        }*/
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Data Set", "Training Data", "The training data set.", GH_ParamAccess.item);
            pManager.AddTextParameter("Feature Names", "Features", "The names of the columns in the data set to be categorized as features. The inputs to a machine learning model are called features.", GH_ParamAccess.list);
            pManager.AddIntegerParameter("Number of Clusters", "Num Clusters", "The number of clusters in the data set.", GH_ParamAccess.item, 2);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Model", "Model", "Trained Model");
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);
            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Clustering Model", SaveModel_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        private void SaveModel_Clicked(object sender, EventArgs e)
        {
            if (trainedModel != null && schema != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Model";
                dialog.Filter = "Zip File (*.zip)|*.zip";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    var mlContext = PG_MLContext.Instance;
                    mlContext.Model.Save(trainedModel, schema, fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid clustering model found. Please create a valid model before trying to save.");
            }
        }
        #endregion

        private ITransformer trainedModel;
        private DataViewSchema schema;
        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> features = new List<string>();
            int numClusters = 2;

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, features) &&
                    DA.GetData<int>(2, ref numClusters)
                )
            {               
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;

                var dataProcessPipeline = mlContext.Transforms.Concatenate("Features", features.ToArray())
                    .Append(mlContext.Clustering.Trainers.KMeans("Features", numberOfClusters: numClusters));

                trainedModel = dataProcessPipeline.Fit(data);
                schema = data.Schema;
                DA.SetData(0, trainedModel);
            }
        }
        #endregion
    }
}



