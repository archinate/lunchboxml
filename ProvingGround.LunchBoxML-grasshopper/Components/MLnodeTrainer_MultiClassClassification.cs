﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using System.Windows.Forms;
using Microsoft.ML.Trainers;
using Microsoft.ML.Trainers.LightGbm;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Multiclass Classification Trainer Component
    /// </summary>
    public class MLnodeTrainer_MultiClassClassfication : GH_Component
    {      
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTrainer_MultiClassClassfication()
            : base("Multiclass Classifier Trainer", "Multiclass Classifier Trainer", "Train a model for multiclass classification problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("d5a8c008-1bc6-42b6-a8ed-2d502863b2a5"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_MulticlassClassifierTrainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Data Set", "Training Data", "The training data set.", GH_ParamAccess.item);
            pManager.AddTextParameter("Feature Names", "Features", "The names of the columns in the data set to be categorized as features. The inputs to a machine learning model are called features.", GH_ParamAccess.list);
            pManager.AddTextParameter("Label Name", "Label", "The name of the column in the data set to be categorized as the label. The ground-truth values (sometimes known as outputs) are used to train a machine learning model are called labels.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The type of trainer used for multiclass classification.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Model", "Model", "Trained Model");
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Multiclass Classification Model", SaveModel_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        private void SaveModel_Clicked(object sender, EventArgs e)
        {
            if (trainedModel != null && schema != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Model";
                dialog.Filter = "Zip File (*.zip)|*.zip";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    var mlContext = PG_MLContext.Instance;
                    mlContext.Model.Save(trainedModel, schema, fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid model found. Please create a valid model before trying to save.");
            }
        }
        #endregion

        private ITransformer trainedModel;
        private DataViewSchema schema;
        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> features = new List<string>();
            string label = "";
            GH_ObjectWrapper _trainer = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, features) &&
                    DA.GetData<string>(2, ref label) &&
                    DA.GetData<GH_ObjectWrapper>(3, ref _trainer) 
                )
            {
                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                IEstimator<ITransformer> learner = null;
                
                try
                {
                    if(_trainer.Value is LbfgsMaximumEntropyTrainer)
                    {
                        LbfgsMaximumEntropyTrainer trainer = _trainer.Value as LbfgsMaximumEntropyTrainer;
                        learner = mlContext.MulticlassClassification.Trainers.LbfgsMaximumEntropy(labelColumnName: "Label",
                            featureColumnName: "Features",
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            optimizationTolerance: trainer.OptimizationTolerance,
                            enforceNonNegativity: trainer.EnforceNonNegativity,
                            historySize: trainer.HistorySize);
                    }
                    else if (_trainer.Value is SdcaMaximumEntropyTrainer)
                    {
                        SdcaMaximumEntropyTrainer trainer = _trainer.Value as SdcaMaximumEntropyTrainer;
                        var options = new SdcaMaximumEntropyMulticlassTrainer.Options
                        {
                            LabelColumnName = "Label",
                            FeatureColumnName = "Features",
                            ConvergenceTolerance = trainer.ConvergenceTolerance,
                            L1Regularization = trainer.L1Regularization,
                            L2Regularization = trainer.L2Regularization,
                            BiasLearningRate = trainer.BiasLearningRate,
                            Shuffle = trainer.Shuffle          
                        };
                        learner = mlContext.MulticlassClassification.Trainers.SdcaMaximumEntropy(options);                  
                    }
                    else if (_trainer.Value is SdcaNonCalibratedTrainer)
                    {
                        SdcaNonCalibratedTrainer trainer = _trainer.Value as SdcaNonCalibratedTrainer;
                        var options = new SdcaNonCalibratedMulticlassTrainer.Options
                        {
                            LabelColumnName = "Label",
                            FeatureColumnName = "Features",
                            ConvergenceTolerance = trainer.ConvergenceTolerance,
                            L1Regularization = trainer.L1Regularization,
                            L2Regularization = trainer.L2Regularization,
                            BiasLearningRate = trainer.BiasLearningRate,
                            Shuffle = trainer.Shuffle
                        };
                        learner = mlContext.MulticlassClassification.Trainers.SdcaNonCalibrated(options);
                    }
                    else if (_trainer.Value is NaiveBayesTrainer)
                    {
                        NaiveBayesTrainer trainer = _trainer.Value as NaiveBayesTrainer;
                        learner = mlContext.MulticlassClassification.Trainers.NaiveBayes();
                    }
                    else if (_trainer.Value is LightGbmTrainer)
                    {
                        LightGbmTrainer trainer = _trainer.Value as LightGbmTrainer;
                        var options = new LightGbmMulticlassTrainer.Options
                        {
                            LabelColumnName = "Label",
                            FeatureColumnName = "Features",
                            EvaluationMetric = trainer.EvaluationMetric,
                            LearningRate = trainer.LearningRate,
                            Sigmoid = trainer.Sigmoid,
                            UseSoftmax = trainer.UseSoftMax,
                            MaximumBinCountPerFeature = trainer.MaxBinsPerFeature,
                            Booster = new DartBooster.Options()
                            {
                                TreeDropFraction = 0.15,
                                XgboostDartMode = false
                            }
                        };
                        learner = mlContext.MulticlassClassification.Trainers.LightGbm(options);
                    }
                }
                catch(Exception e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message);
                }

                if(learner != null)
                {
                    // Define data prep estimator
                    var dataProcessPipeline = mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: label, outputColumnName: "Label")
                    .Append(mlContext.Transforms.Concatenate("Features", features.ToArray()));


                    var trainingPipeline = dataProcessPipeline.Append(learner)
                        .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));
                           
                    trainedModel = trainingPipeline.Fit(data);
                    schema = data.Schema;

                    DA.SetData(0, trainedModel);
                }
            }
        }
        #endregion
    }
}



