﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using System.IO;
using GH_IO.Serialization;
using Grasshopper.Kernel.Types;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Linq;
using Grasshopper.Kernel.Data;
using System.Collections;
using System.Windows.Forms;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeTrainerRegression : GH_Component
    {

        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeTrainerRegression()
            : base("Regression Trainer", "Regression Trainer", "Train a model for regression problems.", "LunchBoxML", "Models")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("83caa148-b897-471d-b020-735b0d1bac9c"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LinearRegressionTrainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Data Set", "Training Data", "The training data set.", GH_ParamAccess.item);
            pManager.AddTextParameter("Feature Names", "Features", "The names of the columns in the data set to be categorized as features. The inputs to a machine learning model are called features.", GH_ParamAccess.list);
            pManager.AddTextParameter("Label Name", "Label", "The name of the column in the data set to be categorized as the label. The ground-truth values (sometimes known as outputs) are used to train a machine learning model are called labels.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The type of trainer used for regression training.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_GenericParam("Model", "Model", "Trained Model");
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Regression Model", SaveModel_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        private void SaveModel_Clicked(object sender, EventArgs e)
        {
            if(trainedModel != null && schema != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Model";
                dialog.Filter = "Zip File (*.zip)|*.zip";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    var mlContext = PG_MLContext.Instance;
                    mlContext.Model.Save(trainedModel, schema, fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid regression model found. Please create a valid model before trying to save.");
            }
        }
        #endregion

        private ITransformer trainedModel;
        private DataViewSchema schema;
        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper _data = new GH_ObjectWrapper();
            List<string> features = new List<string>();
            string label = "";
            GH_ObjectWrapper _trainer = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref _data) &&
                    DA.GetDataList<string>(1, features) &&
                    DA.GetData<string>(2, ref label) &&
                    DA.GetData<GH_ObjectWrapper>(3, ref _trainer)
                )
            {

                var mlContext = PG_MLContext.Instance;
                IDataView data = _data.Value as IDataView;
                IEstimator<ITransformer> learner = null;

                try
                {
                    if (_trainer.Value is FastForestRegressionTrainer)
                    {
                        FastForestRegressionTrainer trainer = _trainer.Value as FastForestRegressionTrainer;
                        learner = mlContext.Regression.Trainers.FastForest(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfTrees: trainer.NumberOfTrees,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf);
                    }
                    else if (_trainer.Value is FastTreeRegressionTrainer)
                    {
                        FastTreeRegressionTrainer trainer = _trainer.Value as FastTreeRegressionTrainer;
                        learner = mlContext.Regression.Trainers.FastTree(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfTrees: trainer.NumberOfTrees,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is FastTreeTweedieRegressionTrainer)
                    {
                        FastTreeTweedieRegressionTrainer trainer = _trainer.Value as FastTreeTweedieRegressionTrainer;
                        learner = mlContext.Regression.Trainers.FastTreeTweedie(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfTrees: trainer.NumberOfTrees,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is GAMRegressionTrainer)
                    {
                        GAMRegressionTrainer trainer = _trainer.Value as GAMRegressionTrainer;
                        learner = mlContext.Regression.Trainers.Gam(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfIterations: trainer.NumberOfIterations,
                            maximumBinCountPerFeature: trainer.MaxBinCountPerFeature,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is LbfgsPoissonRegressionTrainer)
                    {
                        LbfgsPoissonRegressionTrainer trainer = _trainer.Value as LbfgsPoissonRegressionTrainer;
                        learner = mlContext.Regression.Trainers.LbfgsPoissonRegression(labelColumnName: "Label",
                            featureColumnName: "Features",
                            enforceNonNegativity: trainer.EnforceNonNegativity,
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            historySize: trainer.HistorySize,
                            optimizationTolerance: trainer.OptimizationTolerance);
                    }
                    else if (_trainer.Value is LightGBMRegressionTrainer)
                    {
                        LightGBMRegressionTrainer trainer = _trainer.Value as LightGBMRegressionTrainer;
                        learner = mlContext.Regression.Trainers.LightGbm(labelColumnName: "Label",
                            featureColumnName: "Features",
                            numberOfLeaves: trainer.NumberOfLeaves,
                            numberOfIterations: trainer.NumberOfIterations,
                            minimumExampleCountPerLeaf: trainer.MinExampleCountPerLeaf,
                            learningRate: trainer.LearningRate);
                    }
                    else if (_trainer.Value is OLSRegressionTrainer)
                    {
                        OLSRegressionTrainer trainer = _trainer.Value as OLSRegressionTrainer;
                        learner = mlContext.Regression.Trainers.Ols(labelColumnName: "Label",
                            featureColumnName: "Features");
                    }
                    else if (_trainer.Value is OnlineGradientDescentRegressionTrainer)
                    {
                        OnlineGradientDescentRegressionTrainer trainer = _trainer.Value as OnlineGradientDescentRegressionTrainer;
                        learner = mlContext.Regression.Trainers.OnlineGradientDescent(labelColumnName: "Label",
                            featureColumnName: "Features",
                            learningRate: trainer.LearningRate,
                            decreaseLearningRate: trainer.DecreaseLearningRate,
                            l2Regularization: trainer.L2Regularization,
                            numberOfIterations: trainer.NumberOfIterations);
                    }
                    else if (_trainer.Value is SDCARegressionTrainer)
                    {
                        SDCARegressionTrainer trainer = _trainer.Value as SDCARegressionTrainer;
                        learner = mlContext.Regression.Trainers.Sdca(labelColumnName: "Label",
                            featureColumnName: "Features",
                            l1Regularization: trainer.L1Regularization,
                            l2Regularization: trainer.L2Regularization,
                            maximumNumberOfIterations: trainer.NumberOfIterations);
                    }
                }
                catch (Exception e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message);
                }

                if (learner != null)
                {
                    // Define data prep estimator
                    var dataProcessPipeline = mlContext.Transforms.CopyColumns(outputColumnName: "Label", inputColumnName: label)
                    .Append(mlContext.Transforms.Concatenate("Features", features.ToArray()));

                    var trainingPipeline = dataProcessPipeline.Append(learner);

                    trainedModel = trainingPipeline.Fit(data);
                    schema = data.Schema;

                    DA.SetData(0, trainedModel);
                }
            }
        }
        #endregion
    }


}



