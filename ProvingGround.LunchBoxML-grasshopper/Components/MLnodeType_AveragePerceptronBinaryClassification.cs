﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_AveragePerceptronBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_AveragePerceptronBinaryClassification()
            : base("Average Perceptron Binary Classifier", "Avg Perceptron", "The Average Perceptron Binary Classifier is used to train a linear binary classification model trained over boolean label data.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("1637a78b-2802-4dc8-809b-eb8195281d0d"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_AvgPerceptronBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 0.0);
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "Number of passes through the training dataset.", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("Learning Rate", "Learning", "The shrinkage rate for trees, used to prevent over-fitting.", GH_ParamAccess.item, 0.9);
            pManager.AddBooleanParameter("Decrease Learning Rate", "Decrease", "Determine whether to decrease the LearningRate or not. If set to true, The learning rate will be reduced with every weight update proportional to the square root of the number of updates.", GH_ParamAccess.item, false);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double _L2 = 0.0;
            int _numberOfIterations = 1;
            double _learningRate = 0.1;
            bool _decrease = false;

            if (
                    DA.GetData<double>(0, ref _L2) &&
                    DA.GetData<int>(1, ref _numberOfIterations) &&
                    DA.GetData<double>(2, ref _learningRate) &&
                    DA.GetData<bool>(3, ref _decrease)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new AveragedPerceptronBinaryClassificationTrainer(Convert.ToSingle(_learningRate), _decrease, Convert.ToSingle(_L2), _numberOfIterations);
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



