﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_FastTreeRegression : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_FastTreeRegression()
            : base("Fast Tree Regression", "Fast Tree", "The Fast Tree trainer is used for training a decision tree regression model using the Fast Tree method.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("313b151d-50fc-42ca-af2e-9ea990073a86"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_FastTreeRegression; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Number of Leaves", "Leaves", "The max number of leaves in each regression tree.", GH_ParamAccess.item, 20);
            pManager.AddIntegerParameter("Number of Trees", "Trees", "Total number of decision trees to create in the ensemble.", GH_ParamAccess.item, 100);
            pManager.AddIntegerParameter("Minimum Example Count Per Leaf", "Count", "The minimal number of data points required to form a new tree leaf.", GH_ParamAccess.item, 10);
            pManager.AddNumberParameter("Learning Rate", "Learning", "The learning rate.", GH_ParamAccess.item, 0.2);      
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int _numberOfLeaves = 20;
            int _numberOfTrees = 100;
            int _minExampleCountPerLeaf = 10;
            double _learningRate = 0.2;

            if (
                    DA.GetData<int>(0, ref _numberOfLeaves) &&
                    DA.GetData<int>(1, ref _numberOfTrees) &&
                    DA.GetData<int>(2, ref _minExampleCountPerLeaf) &&
                    DA.GetData<double>(3, ref _learningRate)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new FastTreeRegressionTrainer(_numberOfLeaves, _numberOfTrees, _minExampleCountPerLeaf, Convert.ToSingle(_learningRate));
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



