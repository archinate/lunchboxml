﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_GAMRegression : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_GAMRegression()
            : base("Generalized Additive Models Regression", "GAM", "The GAM trainer is used for training a regression model with Generalized Additive Models (GAM).", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("9f285ac9-49be-44c7-9f25-e66b5c22a554"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_GAMRegression; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "The number of iterations to use in learning the features.", GH_ParamAccess.item, 9500);
            pManager.AddIntegerParameter("Maximum Bin Count Per Feature", "Bins", "The maximum number of bins to use to approximate features.", GH_ParamAccess.item, 255);
            pManager.AddNumberParameter("Learning Rate", "Learning", "The learning rate. GAMs work best with a small learning rate.", GH_ParamAccess.item, 0.002);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int _numberOfIterations = 20;
            int _maxBins = 100;
            double _learningRate = 0.2;

            if (
                    DA.GetData<int>(0, ref _numberOfIterations) &&
                    DA.GetData<int>(1, ref _maxBins) &&
                    DA.GetData<double>(2, ref _learningRate)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new GAMRegressionTrainer(_numberOfIterations, _maxBins, Convert.ToSingle(_learningRate));
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



