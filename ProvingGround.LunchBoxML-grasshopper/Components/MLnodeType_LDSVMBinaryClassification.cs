﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_LDSVMBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_LDSVMBinaryClassification()
            : base("Local Deep SVM Binary Classifier", "LDSVM", "The Local Deep SVM Binary Classification trainer is used to train a non-linear binary classification model using the Local Deep SVM method.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("37dc5dc9-8cc5-46ea-9a9b-5cea3577442f"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LDSVMBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "The number of iterations.", GH_ParamAccess.item, 15000);
            pManager.AddIntegerParameter("Tree Depth", "Depth", "The depth of the local depth SVM tree.", GH_ParamAccess.item, 3);
            pManager.AddBooleanParameter("Use Bias", "Bias", "Indicates whether bias should be used or not.", GH_ParamAccess.item, true);
            pManager.AddBooleanParameter("Use Cached Data", "Cache", "Indicated whether or not to use cached data or not.", GH_ParamAccess.item, true);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int _numberOfIterations = 15000;
            int _treeDepth = 3;
            bool _useBias = true;
            bool _useCachedData = true;

            if (
                    DA.GetData<int>(0, ref _numberOfIterations) &&
                    DA.GetData<int>(1, ref _treeDepth) &&
                    DA.GetData<bool>(2, ref _useBias) &&
                    DA.GetData<bool>(3, ref _useCachedData)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new LDSVMBinaryClassificationTrainer(_numberOfIterations, _treeDepth, _useBias, _useCachedData);
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



