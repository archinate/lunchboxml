﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_LbfgsLogisticRegressionBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_LbfgsLogisticRegressionBinaryClassification()
            : base("Lbfgs Logistic Regression Binary Classifier", "Lbfgs Logistic Regression", "The Lbfgs Logistic Regression Binary Classification trainer is used to train linear logistic regression models trained with the L-BFGS method.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("130f8e96-ed6e-4f6f-8a6e-35c9816b5d07"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LBFGSLogisticRegressionBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddBooleanParameter("Enforce Non-Negativity", "Non-Negativity", "Enforce non-negative weights.", GH_ParamAccess.item, false);
            pManager.AddNumberParameter("L1 Regularization", "L1 Regularization", "The L1 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddIntegerParameter("History Size", "History Size", "Number of previous iterations to remember for estimating the Hessian. Lower values mean faster but less accurate estimates.", GH_ParamAccess.item, 20);
            pManager.AddNumberParameter("Optimization Tolerance", "Tolerance", "Tolerance parameter for optimization convergence. (Low = slower, more accurate).", GH_ParamAccess.item, 0.000001);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            bool _enforceNonNegativity = false;
            double _L1Regularization = 1.0;
            double _L2Regularization = 1.0;
            int _historySize = 20;
            double _optimizationTolerance = 0.1;
            if (
                    DA.GetData<bool>(0, ref _enforceNonNegativity) &&
                    DA.GetData<double>(1, ref _L1Regularization) &&
                    DA.GetData<double>(2, ref _L2Regularization) &&
                    DA.GetData<int>(3, ref _historySize) &&
                    DA.GetData<double>(4, ref _optimizationTolerance)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new LbfgsLogisticRegressionBinaryClassificationTrainer(_enforceNonNegativity, Convert.ToSingle(_L1Regularization), Convert.ToSingle(_L2Regularization), _historySize, Convert.ToSingle(_optimizationTolerance));
                DA.SetData(0, trainer);
            }
        }
        #endregion
    }
}



