﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using Microsoft.ML.Trainers.LightGbm;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_LightGbmMulticlass : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_LightGbmMulticlass()
            : base("Light Gbm Multiclass Classifier", "Light Gbm", "The LightGBM trainer is an open source implementation of gradient boosting decision tree.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("e536d215-4a12-400a-ac8e-1dcb885f08c1"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_LightGBMTrainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Evaluation Metric", "Eval Metric", "Determines which evaluation metric to use:\n 1) Default\n 2) Error\n 3) Log Loss\n 4) None", GH_ParamAccess.item, 1);
            pManager.AddNumberParameter("Learning Rate", "Learning Rate", "The shrinkage rate for trees, used to prevent over-fitting. Valid range is [0,1].", GH_ParamAccess.item, 0.9);
            pManager.AddNumberParameter("Sigmoid Parameter", "Sigmoid", "Determines the steepness of the sigmoid function", GH_ParamAccess.item, 0.5);
            pManager.AddBooleanParameter("Use SoftMax", "Use Softmax", "Determines whether to use the SoftMax loss function. The SoftMax function is a generalization of the logistic function to multiple dimensions.", GH_ParamAccess.item, false);
            pManager.AddIntegerParameter("Max Bins Per Feature", "Max Bins", "The maximum number of bins that feature values will be bucketed in. A small number of bins may reduce training accuracy but may increase general power (deal with over-fitting).", GH_ParamAccess.item, 255);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int _evalMetric = 1;
            double _learningRate = 0.9;
            double _sigmoid = 0.5;
            bool _useSoftMax = false;
            int _maxBins = 255;

            if (
                    DA.GetData<int>(0, ref _evalMetric) &&
                    DA.GetData<double>(1, ref _learningRate) &&
                    DA.GetData<double>(2, ref _sigmoid) &&
                    DA.GetData<bool>(3, ref _useSoftMax) &&
                    DA.GetData<int>(4, ref _maxBins)
               )
            {
                if (_evalMetric < 1) _evalMetric = 1;
                else if (_evalMetric > 4) _evalMetric = 4;

                if (_learningRate < 0.0) _learningRate = 0.0;
                else if (_learningRate > 1.0) _learningRate = 1.0;

                if (_maxBins < 2) _maxBins = 2;

                var mlContext = PG_MLContext.Instance;
                var trainer = new LightGbmTrainer((LightGbmMulticlassTrainer.Options.EvaluateMetricType)_evalMetric, Convert.ToSingle(_learningRate), Convert.ToSingle(_sigmoid), _useSoftMax, _maxBins);
                DA.SetData(0, trainer);
            }
        }
        #endregion
    }
}



