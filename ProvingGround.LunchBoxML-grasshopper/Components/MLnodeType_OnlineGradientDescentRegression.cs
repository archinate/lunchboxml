﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_OnlineGradientDescentRegression : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_OnlineGradientDescentRegression()
            : base("Online Gradient Descent Regression", "Online Gradient Descent", "The Online Gradient Descent Trainer is used to train a linear regression model using the online gradient descent method to estimate the parameters for the linear regression model.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("d530efc6-c259-418d-afc1-ed65e74f7254"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_OnlineGradientDescentRegression; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 0.0);
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "Number of passes through the training dataset.", GH_ParamAccess.item, 100);
            pManager.AddNumberParameter("Learning Rate", "Learning", "The shrinkage rate for trees, used to prevent over-fitting.", GH_ParamAccess.item, 0.02);
            pManager.AddBooleanParameter("Decrease Learning Rate", "Decrease", "Determine whether to decrease the LearningRate or not. If set to true, The learning rate will be reduced with every weight update proportional to the square root of the number of updates.", GH_ParamAccess.item, true);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double _L2 = 0.0;
            int _numberOfIterations = 1;
            double _learningRate = 0.1;
            bool _decrease = false;

            if (
                    DA.GetData<double>(0, ref _L2) &&
                    DA.GetData<int>(1, ref _numberOfIterations) &&
                    DA.GetData<double>(2, ref _learningRate) &&
                    DA.GetData<bool>(3, ref _decrease)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new OnlineGradientDescentRegressionTrainer(Convert.ToSingle(_learningRate), _decrease, Convert.ToSingle(_L2), _numberOfIterations);
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



