﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_SDCANoncalibratedBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_SDCANoncalibratedBinaryClassification()
            : base("SDCA Noncalibrated Binary Classifier", "SDCA Noncalibrated", "The SDCA Noncalibrated Binary Classification trainer is used to train logistic regression classification models trained with the stochastic dual coordinate ascent method.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6f75d9f9-014d-4d7d-bfe0-b83c0686a61b"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SDCANoncalibratedBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("L1 Regularization", "L1 Regularization", "The L1 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddIntegerParameter("Maximum Number of Iterations", "Iterations", "The maximum number of passes to perform over the data.", GH_ParamAccess.item, 1);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double _L1Regularization = 1.0;
            double _L2Regularization = 1.0;
            int _iterations = 1;
            if (
                    DA.GetData<double>(0, ref _L1Regularization) &&
                    DA.GetData<double>(1, ref _L2Regularization) &&
                    DA.GetData<int>(2, ref _iterations)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new SDCANoncalibratedBinaryClassificationTrainer(Convert.ToSingle(_L1Regularization), Convert.ToSingle(_L2Regularization), _iterations);
                DA.SetData(0, trainer);
            }
        }
        #endregion
    }
}



