﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_SGDNoncalibratedBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_SGDNoncalibratedBinaryClassification()
            : base("SGD Noncalibrated Binary Classifier", "SGD Noncalibrated", "The SGD Noncalibrated trains a linear classification model using the parallel stochastic gradient descent (SGD) method. The SGD is an iterative algorithm that optimizes a differentiable objective function.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("e83dfbc3-fbc8-4275-aaf3-772c920945be"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SGDNoncalibratedBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 0.0000001);
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "The maximum number of passes through the training dataset; set to 1 to simulate online learning.", GH_ParamAccess.item, 20);
            pManager.AddNumberParameter("Learning Rate", "Learning", "The initial learning rate used by SGD.", GH_ParamAccess.item, 0.01);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double _L2 = 0.0000001;
            int _numberOfIterations = 20;
            double _learningRate = 0.01;

            if (
                    DA.GetData<double>(0, ref _L2) &&
                    DA.GetData<int>(1, ref _numberOfIterations) &&
                    DA.GetData<double>(2, ref _learningRate)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new SGDCalibratedBinaryClassificationTrainer(Convert.ToSingle(_learningRate), Convert.ToSingle(_L2), _numberOfIterations);
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



