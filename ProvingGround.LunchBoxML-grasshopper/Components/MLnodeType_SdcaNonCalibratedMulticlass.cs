﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_SdcaNonCalibratedMulticlass : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_SdcaNonCalibratedMulticlass()
            : base("Sdca Noncalibrated Multiclass Classifier", "Sdca Noncalibrated", "The Sdca noncalibrated model is used to predict a target using a linear multiclass classifier model trained with a coordinate descent method.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("566c22e2-9d73-441f-829c-b8e156f60e58"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SDCANoncalibratedTrainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("Convergence Tolerance", "Convergence", "The tolerance for the ratio between duality gap and primal loss for convergence checking.", GH_ParamAccess.item, 0.1);
            pManager.AddNumberParameter("Bias Learning Rate", "Bias", "The learning rate for adjusting bias from being regularized.", GH_ParamAccess.item, 0.0);
            pManager.AddNumberParameter("L1 Regularization", "L1 Regularization", "The L1 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddNumberParameter("L2 Regularization", "L2 Regularization", "The L2 regularization weight.", GH_ParamAccess.item, 1.0);
            pManager.AddBooleanParameter("Shuffle", "Shuffle", "Determines whether to shuffle data for each training iteration.", GH_ParamAccess.item, true);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            double _convergenceTolerance = 0.1;
            double _L1Regularization = 1.0;
            double _L2Regularization = 1.0;
            double _bias = 0.0;
            bool _shuffle = true;

            if (
                    DA.GetData<double>(0, ref _convergenceTolerance) &&
                    DA.GetData<double>(1, ref _bias) &&
                    DA.GetData<double>(2, ref _L1Regularization) &&
                    DA.GetData<double>(3, ref _L2Regularization) &&
                    DA.GetData<bool>(4, ref _shuffle)
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new SdcaNonCalibratedTrainer(Convert.ToSingle(_convergenceTolerance), Convert.ToSingle(_L1Regularization), Convert.ToSingle(_L2Regularization), Convert.ToSingle(_bias), _shuffle);
                DA.SetData(0, trainer);
            }
        }
        #endregion
    }
}



