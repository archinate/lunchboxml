﻿using System;
using Grasshopper.Kernel;
using ProvingGround.MachineLearning.Classes;
using Grasshopper.Kernel.Types;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using Grasshopper;
using Grasshopper.Kernel.Data;
using System.Drawing;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class MLnodeType_SymbolicSGDLogisticRegressionBinaryClassification : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public MLnodeType_SymbolicSGDLogisticRegressionBinaryClassification()
            : base("Symbolic SGD Logistic Regression Binary Classifier", "Symbolic SGD", "The Symbolic SGD Logistic Regression classifier trains a linear binary classification model using the symbolic stochastic gradient descent (SGD) method. The SGD is an iterative algorithm that optimizes a differentiable objective function.", "LunchBoxML", "Trainer Types")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.secondary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("a4ee8303-24ff-4f9c-8784-d75d20fa3577"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SymbolicSGDBinaryClassification; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddIntegerParameter("Number of Iterations", "Iterations", "Number of passes over the data.", GH_ParamAccess.item, 50);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trainer Type", "Trainer Type", "The trainer used for machine learning problems.", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            int _numberOfIterations = 50;

            if (
                    DA.GetData<int>(0, ref _numberOfIterations) 
               )
            {
                var mlContext = PG_MLContext.Instance;
                var trainer = new SymbolicSGDLogisticRegressionBinaryClassificationTrainer(_numberOfIterations);
                DA.SetData(0, trainer);
            }

        }

        #endregion
    }
}



