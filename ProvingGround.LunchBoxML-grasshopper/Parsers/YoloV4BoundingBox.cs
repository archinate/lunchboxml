﻿using System.Drawing;

namespace ProvingGround.MachineLearning.Parsers
{
    public class YoloV4Result
    {
        /// <summary>
        /// x1, y1, x2, y2 in page coordinates.
        /// <para>left, top, right, bottom.</para>
        /// </summary>
        public float[] BBox { get; }

        /// <summary>
        /// The Bbox category.
        /// </summary>
        public string Label { get; }

        /// <summary>
        /// Confidence level.
        /// </summary>
        public float Confidence { get; }

        /// <summary>
        /// Color of bounding box.
        /// </summary>
        public Color BoxColor { get; set; }

        public YoloV4Result(float[] bbox, string label, float confidence, Color boxColor)
        {
            BBox = bbox;
            Label = label;
            Confidence = confidence;
            BoxColor = boxColor;
        }

        
    }
}
