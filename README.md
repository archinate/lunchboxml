# Announcement: 
Greetings LunchBoxML users,

Version 2.0 of the LunchBoxML tools for Grasshopper are now available! In addition to the prior Accord.NET components, V2 introduces an expansive set of components that rely on [Microsoft's ML.NET framework](https://dotnet.microsoft.com/en-us/apps/machinelearning-ai/ml-dotnet): "An open source and cross-platform machine learning framework". The ML.NET components were originally authored by Andrew Payne while he was with Proving Ground and continue to be used by the team here today - we are excited to see what you can do with them!

Best regards,

Nate Miller

CEO, [Proving Ground](https://provingground.io)


![LunchBoxML-logo.png](LunchBoxML-logo.png)

# What is LunchBoxML? #

LunchBoxML is a Grasshopper extension to [LunchBox](http://provingground.io/tools/lunchbox/) that exposes general purpose machine learning solvers using the [Accord.NET](http://accord-framework.net/) framework

### Who maintains LunchBoxML? ###

* LunchBoxML was originally co-authored by [Nazanin Alsadat Tabatabaei Anaraki](https://www.linkedin.com/in/nazanin-alsadat-tabatabaei-anaraki-63989213a/) and [Nathan Miller](https://provingground.io/about/nathan-miller/) as part of Proving Ground's 2017 [Summer Research internship](https://provingground.io/careers/internships/)
* Andrew Payne developed V2 ML.NET components and greatly expanded options for training and testing of machine learning models.
* LunchBoxML is maintained by [Proving Ground](http://provingground.io)
* If you have questions, visit our [LunchBox Website](http://provingground.io/tools/lunchbox/)

### How do I get started? ###

* Current LunchBoxML builds for Grasshopper are included as part of the official LunchBox installer [Click Once installer link.](http://provingground-lunchbox.s3-website-us-east-1.amazonaws.com/lunchbox.installer.application)
* You can also build the LunchBoxML project from the source code (Visual Studio 2019 C# Project)

Requirements (Rhino):

* [Rhinoceros 7.0 (64-Bit)](https://www.rhino3d.com/)
* [Grasshopper](http://www.grasshopper3d.com/)
* [Accord.NET](http://accord-framework.net/) - LunchBox installer will distribute with the latest version. Conflicting versions from other plugins may create problems with LunchBoxML
* [ML.NET](https://dotnet.microsoft.com/apps/machinelearning-ai/ml-dotnet) - LunchBox installer will distribute with the latest version.
* [.NET 4.7.2 Runtime](https://dotnet.microsoft.com/download/dotnet-framework/net472) - LunchBox installer will distribute with the latest version.


### Are you interested in customizing your own machine learning app? ###
[Check out our software customization service!](https://provingground.io/services/projects/custom-tools-automation/)

### How can I contribute? ###

If you have come across bugs or have wish list items, [submit them to the Issues section of the LunchBoxML repo.](https://bitbucket.org/archinate/LunchBoxML/issues?status=new&status=open)

If you have built some cool stuff for LunchBoxML and would like to share it back with the official project, you can follow these steps...

*  Fork the LunchBoxML repository
*  Make cool stuff.
*  Submit a Pull request.

### GNU Lesser General Public License ###

LunchBoxML is an open source project under the [GNU Lesser General Public](https://www.gnu.org/licenses/lgpl-3.0.en.html) license

Copyright (c) 2023 [Proving Ground LLC](http://ProvingGround.io)

This library is free software; you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or (at 
your option) any later version.

The copyright holders provide no reassurances that the source code provided
does not infringe any patent, copyright, or any other intellectual property
rights of third parties. The copyright holders disclaim any liability to 
any recipient for claims brought against recipient by any third party for 
infringement of that parties intellectual property rights.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
for more details.